# II. Servicer le programme

**Première chose, plutôt que de lancer ce programme à la main, on va en faire un *service*.**  
Dans les OS GNU/Linux modernes, c'est *systemd* qui gère les services : il les lance, les relance s'ils crashent, les surveille, etc.

> *systemd* est un programme présent par défaut dans les OS GNU/Linux modernes. La commande `systemctl` permet d'ordonner des choses à *systemd* comme le démarrage d'un *service*.

Un *service* c'est simplement un programme que *systemd* va lancer (ce ne sera pas l'utilisateur qui le lancera à la main).  
Quand on utilise des commandes `systemctl` pour démarrer des machins, ces machins, c'est des *services*.

Notre but dans cette section : **créer un service `efrei_server.service` qui lance le programme `efrei_server`**.

> Vous allez voir que ça va nous permettre rapidement plein de choses, faciliter l'administration mais aussi améliorer le niveau de sécurité de l'application. *systemd* ne se contente pas de lancer le programme à notre place, mais peut faire des choses bien plus avancées si on lui demande.

![systemd service](./img/systemd_service.png)

## Sommaire

- [II. Servicer le programme](#ii-servicer-le-programme)
  - [Sommaire](#sommaire)
  - [1. Création du service](#1-création-du-service)
  - [2. Tests](#2-tests)

## 1. Création du service

🌞 **Créer un service `efrei_server.service`**

- pour cela, il faut créer le fichier suivant : `/etc/systemd/system/efrei_server.service`
- avec le contenu (simpliste) suivant :

> *Vous pouvez le nommer autrement (ou pas) parce que `efrei_server` c kan meme super niul kom nom.*

```systemd
[Unit]
Description=Super serveur EFREI

[Service]
ExecStart=/usr/local/bin/efrei_server/efrei_server
EnvironmentFile=/usr/local/bin/efrei_server/envfile.env
```

```env
LISTEN_ADDRESS=192.168.56.105
LOG_DIR=/tmp/efrei_server

```

## 2. Tests

🌞 **Exécuter la commande `systemctl status efrei_server`**

- le nom qu'on tape ici : `efrei_server`, c'est le nom du fichier
- vous devriez voir que votre service est inactif
```bash
[pastoufle@efrei-xmg4agau1 efrei_server]$ systemctl status efrei_server
○ efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: inactive (dead)

```

🌞 **Démarrer le service**

- avec une commande `systemctl` adaptée

```bash
[pastoufle@efrei-xmg4agau1 efrei_server]$ sudo systemctl start
```

🌞 **Vérifier que le programme tourne correctement**

- avec une commande `systemctl` adaptée, afficher le statut du *service* `efrei_server`
```bash
efrei_server.service
[pastoufle@efrei-xmg4agau1 efrei_server]$ sudo systemctl status efrei_server.service
● efrei_server.service - Super serveur EFREI
     Loaded: loaded (/etc/systemd/system/efrei_server.service; static)
     Active: active (running) since Tue 2024-09-10 15:14:45 CEST; 21s ago
   Main PID: 2908 (efrei_server)
      Tasks: 2 (limit: 31666)
     Memory: 32.5M
        CPU: 152ms
     CGroup: /system.slice/efrei_server.service
             ├─2908 /usr/local/bin/efrei_server/efrei_server
             └─2909 /usr/local/bin/efrei_server/efrei_server

Sep 10 15:14:45 efrei-xmg4agau1.etudiants.campus.villejuif systemd[1]: Started Super serveur EFREI.
```
- avec une commande `ss` adaptée, prouver que le programme écoute sur l'adresse IP souhaitée
```bash
[pastoufle@efrei-xmg4agau1 efrei_server]$ ss -ltdn
Netid        State         Recv-Q        Send-Q                Local Address:Port               Peer Address:Port        Process        
tcp          LISTEN        0             100                  192.168.56.105:8888                    0.0.0.0:*                          
tcp          LISTEN        0             128                         0.0.0.0:22                      0.0.0.0:*                          
tcp          LISTEN        0             128                            [::]:22                         [::]:*                          

```
- depuis votre PC, connectez-vous au service, en utilisant une commande `nc`

```bash
pastoufle@H4CK3R:~$ nc 192.168.56.105 8888
Hello ! Tu veux des infos sur quoi ?
1) cpu
2) ram
3) disk
4) ls un dossier

Ton choix (1, 2, 3 ou 4) : 
```

> ➜ [**Lien vers la partie 3**](./part3.md)
