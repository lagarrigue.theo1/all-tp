# TP1 : Héberger un service


# Sommaire

- [TP1 : Héberger un service](#tp1--héberger-un-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [Suite du TP](#suite-du-tp)

# 0. Prérequis

➜ **Rendu avec un dépôt Git**

- rendu format Markdown donc
- créez-le tout de suite votre dépôt
- et envoyez-moi un lien par MP Discord avec votre nom de famille et l'URL vers votre dépôt

➜ **Dès que vous voyez l'emoji 🌞 c'est qu'il y a quelque chose à faire qui doit figurer dans le rendu**, des commandes à taper par ex

➜ **Une VM avec un OS Linux de votre choix**

- je vous recommande une base RedHat comme Rocky Linux, comme d'hab
- mais vous êtes libres, si vous souhaitez travailler avec un autre OS
- pas de GUI pour votre VM évidemment !

➜ **La VM doit avoir...**

- une interface réseau pour **accéder à internet** (NAT avec VirtualBox)
- une interface réseau pour **accéder à un réseau local avec le PC hôte** (host-only dans VirtualBox)
- un **accès SSH fonctionnel**, pour que vous puissiez l'administrer
- un **firewall activé**, qui bloque toutes les connexions entrantes par défaut (à part SSH)

🌞 **Boom ça commence direct : je veux l'état initial du firewall**

- dans le compte-rendu, une commande qui me montre l'état initial de votre firewall (qui ne doit autoriser que le trafic SSH en entrée)
  
```bash
[pastoufle@efrei-xmg4agau1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules:
```

➜ **Gestion d'utilisateurs**

- la machine doit posséder un user qui porte votre prénom ou pseudo ou truc du genre
- il est interdit d'utiliser `root` directement
- si besoin des privilèges de `root`, vous utiliserez exclusivement la commande `sudo`

🌞 **Fichiers /etc/sudoers /etc/passwd /etc/group** dans le dépôt de compte-rendu svp !

Fait !

![Sudo](./img/pingu.jpg)

# Suite du TP

Vous pouvez enchaîner sur les 3 parties du TP, dans l'ordre :

- [**Partie 1** : Host & Hack](./part1.md)
- [**Partie 2** : Servicer le programme](./part2.md)
- [**Partie 3** : MAKE SERVICES GREAT AGAIN](./part3.md)
- [**Partie 4** : Autour de l'application](./part4.md)

> *Ui le verbe "servicer" existe po. J'dois écrire que des trucs qui ont du sens maintenant ?*
