# III. MAKE SERVICES GREAT AGAIN

Bon le programme est lancé en tant que *service*, mais il est toujours bien pété et les risques sont toujours les mêmes en cas d'exploitation.

**On va prendre plusieurs mesures :**

- proposer une politique de **restart automatique**
- maîtriser **l'emplacement et les permissions** des fichiers liés à l'application
- lancer l'app en tant qu'un **utilisateur** spécifique qui n'a que peu de privilèges sur la machine
- **empêcher les connexions réseau** intempestives (il peut recevoir des clients sur un port spécifique uniquement)
- protéger l'app contre le **flood**
- **isoler** l'application du reste du système
- empêcher le service de faire **des actions indésirables**

**A la fin on aura un programme vulnérable qui :**

- ne pourra pas lancer d'autre programmes
- n'aura aucun droit de lecture/écriture à part là où il a rigoureusement besopin
- sera isolé du reste du système (il ne PEUT PAS accéder à certaines ressources du système)
- sera surveillé
- pourra accéder au réseau de façon très limitée
- sera protégé des attaques par spam trop sommaires

➜ **Autrement dit** : on sera chills, et on pourra aller engueuler les dévs de proposer des trucs tout pourris comme ça.

![Linux Hardening](./img/linux_hardening.png)

## Sommaire

- [III. MAKE SERVICES GREAT AGAIN](#iii-make-services-great-again)
  - [Sommaire](#sommaire)
  - [1. Restart automatique](#1-restart-automatique)
  - [2. Utilisateur applicatif](#2-utilisateur-applicatif)
  - [3. Maîtrisez l'emplacement des fichiers](#3-maîtrisez-lemplacement-des-fichiers)
  - [4. Security hardening](#4-security-hardening)

## 1. Restart automatique

Bon pour ça, facile, on va juste faire en sorte que si le programme coupe, il soit relancé automatiquement.

🌞 **Ajoutez une clause dans le fichier `efrei_server.service` pour le restart automatique**

- c'est la clause `Restart=`
- trouvez la valeur adaptée pour qu'il redémarre tout le temps, dès qu'il est coupé
```bash
Restart=always
```

🌞 **Testez que ça fonctionne**

- lancez le *service* avec une commande `systemctl`
- affichez le processus lancé par *systemd* avec une commande `ps`
  - je veux que vous utilisiez une commande avec `| grep quelquechose` pour n'afficher que la ligne qui nous intéresse
  - vous devriez voir un processus `efrei_server` qui s'exécute
```bash
[pastoufle@efrei-xmg4agau1 ~]$ ps -e | grep efrei
   3031 ?        00:00:00 efrei_server
```
- tuez le processus manuellement avec une commande `kill`
- constatez que :
  - le service a bien été relancé
  - il y a bien un nouveau processus `efrei_server` qui s'exécute
```bash
[pastoufle@efrei-xmg4agau1 ~]$ sudo kill 3031
[pastoufle@efrei-xmg4agau1 ~]$ ps -e | grep efrei
   3115 ?        00:00:00 efrei_server

```

> Pour rappel, **TOUTES** les commandes pour faire ce qui est demandé avec un 🌞 doivent figurer dans le compte-rendu.

## 2. Utilisateur applicatif

Lorsqu'un programme s'exécute sur une machine (peu importe l'OS ou le contexte), le programme est **toujours** exécuté sous l'identité d'un utilisateur.  
Ainsi, pendant son exécution, le programme aura les droits de cet utilisateur.  

> Par exemple, un programme lancé en tant que `toto` pourra lire un fichier `/var/log/toto.log` uniquement si l'utilisateur `toto` a les droits sur ce fichier.

🌞 **Créer un utilisateur applicatif**

- c'est lui qui lancera `efrei_server`
- avec une commande `useradd`
- choisissez...
  ```bash
  sudo useradd -m -d /var/lib/efrei_user -s /usr/sbin/nologin efrei_user
  ```

> N'hésitez pas à venir vers moi pour discuter de ce qui est le plus "approprié" si nécessaire.

🌞 **Modifier le service pour que ce nouvel utilisateur lance le programme `efrei_server`**

- je vous laisse chercher la clause appropriée à ajouter dans le fichier `.service`
```bash
User=efrei_user
Group=efrei_user
```

🌞 **Vérifier que le programme s'exécute bien sous l'identité de ce nouvel utilisateur**

- avec une commande `ps`
- encore là, filtrez la sortie avec un `| grep`
- n'oubliez pas de redémarrer le service pour que ça prenne effet hein !

> *Déjà à ce stade, le programme a des droits vraiment limités sur le système.*
>
```bash
[pastoufle@efrei-xmg4agau1 ~]$ ps -ef | grep efrei
efrei_u+    2030       1  0 11:35 ?        00:00:00 /usr/local/bin/efrei_server/efrei_server
efrei_u+    2031    2030  0 11:35 ?        00:00:00 /usr/local/bin/efrei_server/efrei_server
```

## 3. Maîtrisez l'emplacement des fichiers

Pour fonctionner, l'application a besoin de deux choses :

- des **variables d'environnement définies**, ou des valeurs par défaut nulles seront utilisées
- un **fichier de log** où elle peut écrire
  - par défaut elle écrit dans `/tmp` comme l'indique le warning au lancement de l'application
  - vous pouvez définir la variable `LOG_DIR` pour choisir l'emplacement du fichier de logs

🌞 **Choisir l'emplacement du fichier de logs**

- créez un dossier dédié dans `/var/log/` (le dossier standard pour stocker les logs)
- indiquez votre nouveau dossier de log à l'application avec la variable `LOG_DIR`
- l'application créera un fichier `server.log` à l'intérieur

```bash
LOG_DIR=/var/log/efrei_server
```

🌞 **Maîtriser les permissions du fichier de logs**

- avec les commandes `chown` et `chmod`
- appliquez les permissions les plus restrictives possibles sur le dossier dans `var/log/`

![chown chmod](./img/chown-chmod-2.webp)

```bash
sudo chown efrei_user efrei_server/

sudo chmod 700 efrei_server/
-rwx------.  2 efrei_user efrei_user      24 Sep 13 11:35 efrei_server
```

## 4. Security hardening

Il existe beaucoup de clauses qu'on peut ajouter dans un fichier `.service` pour que *systemd* s'occupe de sécuriser le service, en l'isolant du reste du système par exemple.

Ainsi, une commande est fournie `systemd-analyze security` qui permet de voir quelles mesures de sécurité on a activé. Un score (un peu arbitraire) est attribué au *service* ; cela représente son "niveau de sécurité".

Cette commande est **très** pratique d'un point de vue pédagogique : elle va vous montrer toutes les clauses qu'on peut ajouter dans un `.service` pour renforcer sa sécurité.

🌞 **Modifier le `.service` pour augmenter son niveau de sécurité**

```
ProtectSystem=full
NoNewPrivileges=true
ProtectHome=true
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
ProtectKernelModules=true
```

🌟 **BONUS : Essayez d'avoir le score le plus haut avec `systemd-analyze security`**

```
efrei_server.service                      6.0 MEDIUM    😐
```
➜ 💡💡💡 **A ce stade, vous pouvez ré-essayez l'injection que vous avez trouvé dans la partie 1. Normalement, on peut faire déjà moins de trucs avec.**

> ➜ [**Lien vers la partie 4**](./part4.md)
