# B2 EFREI Cyber 2023 Gestion d'infrastructure Cloud

> *c lon kom titr*

Ici vous trouverez tous les supports de cours, TPs et autres ressources liées au cours.

# TPs

- [TP1 : Programmatic provisioning](./tp/1/README.md)
- [TP2 : Network boot](./tp/2/README.md)

![Cloud](./img/cloud.png)
