# II. 3. Setup réseau

![VXLAN](./img/vxlan_bornt.png)


## Sommaire

- [II. 3. Setup réseau](#ii-3-setup-réseau)
  - [Sommaire](#sommaire)
  - [A. Intro](#a-intro)
  - [B. Création du Virtual Network](#b-création-du-virtual-network)
  - [C. Préparer le bridge réseau](#c-préparer-le-bridge-réseau)

## A. Intro

## B. Création du Virtual Network

## C. Préparer le bridge réseau

🌞 **Créer et configurer le bridge Linux**, j'vous file tout, suivez le guide :

```bash
sudo ip link add name vxlan_bridge type bridge

sudo ip link set dev vxlan_bridge up 

sudo ip addr add 10.220.220.201/24 dev vxlan_bridge

sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent

sudo firewall-cmd --add-masquerade --permanent

sudo firewall-cmd --reload
```
