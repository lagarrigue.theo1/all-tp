# II.1. Setup Frontend

![Frontend](./img/frontend.png)

Le Frontend est la machine qui contiendra la logique de l'application et qui va piloter les noeuds KVM.

**Toute cette partie est à réaliser uniquement sur `frontend.one`.**

## Sommaire

- [II.1. Setup Frontend](#ii1-setup-frontend)
  - [Sommaire](#sommaire)
  - [A. Database](#a-database)
  - [B. OpenNebula](#b-opennebula)
  - [C. Conf système](#c-conf-système)

## A. Database

🌞 **Installer un serveur MySQL**

- on va installer une version spécifique de MySQL, demandée par OpenNebula
- télécharger le RPM disponible à l'URL suivante : https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm 
- puis installez-le localement avec une commande `rpm`

> Quand vous faites une commande `dnf install`, ça télécharge un `.rpm` (qui est juste une archive qui contient plein de fichiers) et ça l'installe automatiquement ("installer" ça veut juste dire qu'on extrait le contenu de l'archive et on dispose chaque fichier au "bon" endroit sur le système.)

```
wget https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm 
rpm -i mysql80-community-release-el9-5.noarch.rpm 

```

🌞 **Démarrer le serveur MySQL**
```
- sudo systemctl start mysqld
- sudo systemctl enable mysqld
```
🌞 **Setup MySQL**

```SQL
mysql -u root -p

ALTER USER 'root'@'localhost' IDENTIFIED BY 'hey_boi_define_a_strong_password';
CREATE USER 'oneadmin' IDENTIFIED BY 'also_here_define_another_strong_password';
CREATE DATABASE opennebula;
GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
```

## B. OpenNebula

🌞 **Ajouter les dépôts Open Nebula**

```
vim /etc/yum.repos.d/opennebula.repo

[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/6.8/RedHat/$releasever/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo2.key
gpgcheck=1
repo_gpgcheck=1

dnf makecache -y
```

🌞 **Installer OpenNebula**

```
- dnf install opennebula opennebula-sunstone opennebula-fireedge
```

🌞 **Configuration OpenNebula**

```conf
vim /etc/one/oned.conf

DB = [ BACKEND = "mysql",
       SERVER  = "localhost",
       PORT    = 0,
       USER    = "oneadmin",
       PASSWD  = "also_here_define_another_strong_password",
       DB_NAME = "opennebula",
       CONNECTIONS = 25,
       COMPARE_BINARY = "no" ]
```

🌞 **Créer un user pour se log sur la WebUI OpenNebula**

```
sudo su - oneadmin

vim var/lib/one/.one/one_auth

[oneadmin@frontend ~]$ cat /var/lib/one/.one/one_auth
oneadmin:password
```

🌞 **Démarrer les services OpenNebula**

```
sudo systeme start opennebula
sudo systeme start opennebula-sunstone
sudo systeme enable opennebula 
sudo systeme enable opennebula-sunstone
```

## C. Conf système

🌞 **Ouverture firewall**

```
sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
sudo firewall-cmd --zone=public --add-port=9869/tcp --permanent
sudo firewall-cmd --zone=public --add-port=2633/tcp --permanent
sudo firewall-cmd --zone=public --add-port=4124/tcp --permanent
sudo firewall-cmd --zone=public --add-port=4124/udp --permanent
sudo firewall-cmd --zone=public --add-port=29876/tcp --permanent
sudo firewall-cmd --reload
```
