# II. 2. Noeuds KVM

![KVM bare-metal](./img/bare_kvm.png)

## Sommaire

- [II. 2. Noeuds KVM](#ii-2-noeuds-kvm)
  - [Sommaire](#sommaire)
  - [A. KVM](#a-kvm)
  - [B. Système](#b-système)

## A. KVM

🌞 **Ajouter des dépôts supplémentaires**

```
vim /etc/yum.repos.d/opennebula.repo

[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/6.8/RedHat/$releasever/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo2.key
gpgcheck=1
repo_gpgcheck=1

dnf makecache -y

dnf install -y epel-release
```


🌞 **Installer KVM**

```
sudo dnf install opennebula-node-kvm`
```

🌞 **Démarrer le service `libvirtd`**

```
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
```

## B. Système

🌞 **Ouverture firewall**

```
sudo firewall-cmd --zone=public --add-port=22/tcp --permanent
sudo firewall-cmd --zone=public --add-port=8472/udp --permanent
sudo firewall-cmd --reload
```
