# TP1 : Programmatic provisioning

## Sommaire

- [TP1 : Programmatic provisioning](#tp1--programmatic-provisioning)
  - [Sommaire](#sommaire)
- [I. Une première VM](#i-une-première-vm)
  - [1. ez startup](#1-ez-startup)
  - [2. Un peu de conf](#2-un-peu-de-conf)
- [II. Initialization script](#ii-initialization-script)
- [III. Repackaging](#iii-repackaging)
- [IV. Multi VM](#iv-multi-vm)
- [V. cloud-init](#v-cloud-init)

# I. Une première VM

## 1. ez startup


🌞 **`Vagrantfile` dans le dépôt git de rendu SVP !**

 **Vagrantfile_EZ_startup**

## 2. Un peu de conf

Avec Vagrant, il est possible de gérer un certains nombres de paramètres de la VM.

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

- ait l'IP `10.1.1.11/24`
- porte le hostname `ezconf.tp1.efrei`
- porte le nom (pour Vagrant) `ezconf.tp1.efrei` (ce n'est pas le hostname de la machine)
- ait 2G de RAM
- ait un disque dur de 20G

```
Vagrant.configure("2") do |config|
  config.vm.network "private_network", ip: "10.1.1.11"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end
  config.vm.define "ezconf.tp1.efrei"
  config.vm.box = "generic/rocky9"
  config.disksize.size = '20GB'
end
```

# II. Initialization script

**script.sh**
```bash
#!/bin/bash

#sudo dnf update -y
sudo dnf install -y vim
sudo dnf install -y python3
```

**Vagrantfile**

```Vagrantfile
config.vm.provision "shell", path: "script.sh" 
```

# III. Repackaging
```bash
$ vagrant package --output rocky-efrei.box

$ vagrant box add rocky-efrei rocky-efrei.box

$ vagrant box list
```

# IV. Multi VM

```Vagrantfile
machines=[
  {
    :hostname => "node1.tp1.efrei",
    :ip => "10.1.1.101",
    :box => "generic/rocky9",
    :ram => 2048,
  },
  {
    :hostname => "node2.tp1.efrei",
    :ip => "10.1.1.102",
    :box => "generic/rocky9",
    :ram => 1058,
  }
]

Vagrant.configure(2) do |config|
    machines.each do |machine|
        config.vm.define machine[:hostname] do |node|
            node.vm.box = machine[:box]
            node.disksize.size = '20GB'
            node.vm.hostname = machine[:hostname]
            node.vm.network "private_network", ip: machine[:ip]
            node.vm.provider "virtualbox" do |vb|
                vb.memory = machine[:ram]
            end
        end
    end
end
```

```bash
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=1.20 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=2.15 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=1.82 ms
64 bytes from 10.1.1.102: icmp_seq=4 ttl=64 time=5.69 ms
64 bytes from 10.1.1.102: icmp_seq=5 ttl=64 time=6.64 ms
64 bytes from 10.1.1.102: icmp_seq=6 ttl=64 time=1.91 ms

^C
--- 10.1.1.102 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5012ms
rtt min/avg/max/mdev = 1.201/3.234/6.637/2.108 ms
```
# V. cloud-init

🌞 **Repackager une box Vagrant**
  ```
  $ vagrant.exe box list
  generic/rocky9 (virtualbox, 4.3.12, (amd64))
  rocky-efrei    (virtualbox, 0)
  rocky-efrei2   (virtualbox, 0)
  ```

🌞 **Tester !**

```
machines=[
  {
    :hostname => "node1.tp1.efrei",
    :ip => "10.1.1.101",
    :box => "rocky-efrei2",
    :ram => 2048,
  }
#  {
#    :hostname => "node2.tp1.efrei",
#    :ip => "10.1.1.102",
#    :box => "generic/rocky9",
#    :ram => 1058,
#  }
]

Vagrant.configure(2) do |config|
    machines.each do |machine|
        config.vm.define machine[:hostname] do |node|
            node.vm.box = machine[:box]
                node.vm.disk :dvd, name: "prout-t", file: "./cloud-init.iso"
            node.disksize.size = '20GB'
            node.vm.hostname = machine[:hostname]
            node.vm.network "private_network", ip: machine[:ip]
            node.vm.provider "virtualbox" do |vb|
                vb.memory = machine[:ram]
            end
        end
    end
end

```