# TP1 : Containers

# I. Docker
🌞 **Utiliser la commande `docker run`**

# commande docker run:
```bash
sudo docker run -d -p 8080:80 --name tpdocker -v /var/tp1/conf:/etc/nginx/conf.d -v /var/tp1/html:/usr/share/nginx/html --cpus="0.5" --memory="256m" nginx
```

# fichier html:
```bash
aaaa
```

# fichier conf:
```bash
server {
    listen 80;
    server_name localhost;

    location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
    }

    error_page 500 502 503 504  /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }
}
```

🌞 **Construire votre propre image**

# Dockerfile
```bash
FROM debian:10

RUN apt update -y
RUN apt install -y apache2

RUN mkdir -p /etc/apache2/logs/

COPY index.html /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf

CMD ["apache2ctl", "-D", "FOREGROUND"]
```

# apache2.conf
```bash
Listen 80

LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

DirectoryIndex index.html
DocumentRoot "/var/www/html/"

ErrorLog "logs/error.log"
LogLevel warn

ServerName localhost
```

# lancer image
```bash
docker build . -t customimage
docker run -d -p 8080:80 customimage
```

## 3. Make your own app

# Dockerfile
```bash                                                
FROM python:3

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

EXPOSE 8888

CMD ["python", "app.py"]
```

# docker-compose.yml
```bash
version: '3'

services:
  app:
    build: .
    ports:
      - "8888:8888"
    container_name: "app"
    depends_on:
      - db

  db:
    image: "redis:latest"
    ports:
      - "6379:6379"
    container_name: "db"
```

# architecture 
```bash
[pastoufle@localhost app]$ ls
app.py  docker-compose.yml  Dockerfile  requirements.txt  templates

[pastoufle@localhost app]$ pwd
/home/pastoufle/TpDocker/app
```


# IV. Docker security

Dans cette partie, on va survoler quelques aspects de Docker en terme de sécurité.

## 1. Le groupe docker

🌞 **Prouvez que vous pouvez devenir `root`**

```bash
docker run -v /etc/shadow:/etc/shadow2 debian cat /etc/shadow2
```

## 2. Scan de vuln

Il existe des outils dédiés au scan de vulnérabilités dans des images Docker.

C'est le cas de [Trivy](https://github.com/aquasecurity/trivy) par exemple.

🌞 **Utilisez Trivy**
# Scan app python
```bash
trivy image app-app

app-app (debian 12.2)
Total: 780 (UNKNOWN: 2, LOW: 500, MEDIUM: 219, HIGH: 56, CRITICAL: 3)

Python (python-pkg)
Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 1, HIGH: 0, CRITICAL: 0)
```
# Scan bdd app
```bash
trivy image redis:latest

redis:latest (debian 12.2)
Total: 184 (UNKNOWN: 1, LOW: 136, MEDIUM: 31, HIGH: 15, CRITICAL: 1)

usr/local/bin/gosu (gobinary)
Total: 4 (UNKNOWN: 0, LOW: 1, MEDIUM: 2, HIGH: 1, CRITICAL: 0)
```
# Scan Apache
```bash
trivy image customimage

customimage (debian 10.13)
Total: 241 (UNKNOWN: 1, LOW: 161, MEDIUM: 33, HIGH: 44, CRITICAL: 2)

/etc/ssl/private/ssl-cert-snakeoil.key (secrets)
Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 1, CRITICAL: 0)
```

# Scan nginx
```bash
trivy image nginx

nginx (debian 12.2)
Total: 110 (UNKNOWN: 2, LOW: 81, MEDIUM: 19, HIGH: 7, CRITICAL: 1)
```

## 3. Petit benchmark secu
🌞 **Utilisez l'outil Docker Bench for Security**

  sur ma machine de test 
  ```bash
  Section C - Score
  [INFO] Checks: 86
  [INFO] Score: -1
  ```
