# Partie 1 : Premiers pas Azure


# I. Premiers pas

🌞 **Créez une VM depuis la WebUI**
  ```bash
  ssh pastoufle@4.210.81.110

  pastoufle@VM1:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0d:3a:b6:02:63 brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.4/24 metric 100 brd 10.1.1.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::20d:3aff:feb6:263/64 scope link 
       valid_lft forever preferred_lft forever
  ```

🌞 **Créez une VM depuis le Azure CLI**

```bash
vm create --name VM2 --resource-group Efrei --image Ubuntu2204 --admin-username pastoufle --ssh
&
{
  "fqdns": "",
  "id": "/subscriptions/9ac17899-3bab-40ad-8c50-41e1ea589515/resourceGroups/Efrei/providers/Microsoft.Compute/virtualMachines/VM2",
  "location": "northeurope",
  "macAddress": "00-22-48-9D-60-09",
  "powerState": "VM running",
  "privateIpAddress": "10.1.1.5",
  "publicIpAddress": "13.79.24.216",
  "resourceGroup": "Efrei",
  "zones": ""
}

```

🌞 **Créez deux VMs depuis le Azure CLI**

```bash
pastoufle@VM1:~$ ping 10.1.1.5
PING 10.1.1.5 (10.1.1.5) 56(84) bytes of data.
64 bytes from 10.1.1.5: icmp_seq=1 ttl=64 time=1.07 ms
64 bytes from 10.1.1.5: icmp_seq=2 ttl=64 time=1.21 ms
64 bytes from 10.1.1.5: icmp_seq=3 ttl=64 time=1.16 ms
--- 10.1.1.5 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.070/1.147/1.213/0.058 ms
```

# II. cloud-init

🌞 **Tester `cloud-init`**
```yml
[cloud] ❱❱❱ cat cloud-init.txt 
#cloud-config
users:
  - name: pastoufle
    primary_group: pastoufle
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$abpoPsy31MgieVIA$CLXKiAMF5mP7ckPMzMvtpSWRwObb6LaqUb1mL9xQsxU.cYCDUV0XNS2ghmBUnm4aPybRyFu6i1A14ktzB6F6V/
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDbZCbvFRU/HB0+q87Cq0RlRuK04YhohJu2u1tV8FmWUe2ZOUBSZnK2EjlqA10896W+kwAq+sYYi40c8KSTsE6rphuYTcEyarjgoGxrb2+WEEeTOSMStK/VnisHlIZaZctGgqChfNfEgX/Cre6kfKV4Yl+gXi3ETuA09Nd8HtyOYrbjpT/j77t2cMRh+o4JjTtUPdd8QgeUchluZd136BAudHV8Qavv6HfjImSZ1N4ABCDR7ecliCDM/RlQUTv7BI4XDg2BmmmBmnZJkvGgVbG3Hnb5uSqmeWsxqYqmyBHBDAE8/VLrLx2pNPOVtZHWHuyjSoK9re2R6afL+jeMp12z4+nI3VRphqVtqY9hqrmdKDVekM4GG/JjCJt03Q7K9uVcYLfeqLV0OfcOzOe/httnn4m4k8H6Vjb44vfWPFqMx2ql7WI6ub32K62S++0j9J9HgsW5EyK7MMpnbiWBPdZ88eWVX1E3nrXAPELUQrCQldV6ZqZsHZdJg050xfYj4vPHIi4HYocJ/FhWX6+ImLx6vY3onBbZ1qxbS/OrlKSKEJcs/ATjytvzqcsoFQ3VEcZCuH2/+zg3+ZZmKnc/gWBuOCQkKQEqcWA3vHhIPHwn6wKpQxueOKz2bGJaSfTqmT94zDsjXmA/kKW7kqWJCXsPMRM9bqpCcvBhFW8H3bjcew== pastoufler@HackerLol
```

```bash
vm create --name VM2 --resource-group Efrei --image Ubuntu2204 --custom-data /home/pastoufler/cours/cloud/cloud-init.txt
{
  "fqdns": "",
  "id": "/subscriptions/9ac17899-3bab-40ad-8c50-41e1ea589515/resourceGroups/Efrei/providers/Microsoft.Compute/virtualMachines/VM3",
  "location": "northeurope",
  "macAddress": "00-0D-3A-DE-7E-D2",
  "powerState": "VM running",
  "privateIpAddress": "10.1.1.6",
  "publicIpAddress": "20.107.196.3",
  "resourceGroup": "Efrei",
  "zones": ""
}
```

🌞 **Vérifier que `cloud-init` a bien fonctionné**

```bash
pastoufle@VM3:~$ less /etc/passwd|grep "/bin/bash"
root:x:0:0:root:/root:/bin/bash
pastoufle:x:1000:1000::/home/pastoufle:/bin/bash
```

```bash
pastoufle@VM3:~/.ssh$ cat authorized_keys 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDbZCbvFRU/HB0+q87Cq0RlRuK04YhohJu2u1tV8FmWUe2ZOUBSZnK2EjlqA10896W+kwAq+sYYi40c8KSTsE6rphuYTcEyarjgoGxrb2+WEEeTOSMStK/VnisHlIZaZctGgqChfNfEgX/Cre6kfKV4Yl+gXi3ETuA09Nd8HtyOYrbjpT/j77t2cMRh+o4JjTtUPdd8QgeUchluZd136BAudHV8Qavv6HfjImSZ1N4ABCDR7ecliCDM/RlQUTv7BI4XDg2BmmmBmnZJkvGgVbG3Hnb5uSqmeWsxqYqmyBHBDAE8/VLrLx2pNPOVtZHWHuyjSoK9re2R6afL+jeMp12z4+nI3VRphqVtqY9hqrmdKDVekM4GG/JjCJt03Q7K9uVcYLfeqLV0OfcOzOe/httnn4m4k8H6Vjb44vfWPFqMx2ql7WI6ub32K62S++0j9J9HgsW5EyK7MMpnbiWBPdZ88eWVX1E3nrXAPELUQrCQldV6ZqZsHZdJg050xfYj4vPHIi4HYocJ/FhWX6+ImLx6vY3onBbZ1qxbS/OrlKSKEJcs/ATjytvzqcsoFQ3VEcZCuH2/+zg3+ZZmKnc/gWBuOCQkKQEqcWA3vHhIPHwn6wKpQxueOKz2bGJaSfTqmT94zDsjXmA/kKW7kqWJCXsPMRM9bqpCcvBhFW8H3bjcew== pastoufler@HackerLol
```
