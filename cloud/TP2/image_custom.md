# Partie 2 : Créer et utiliser une image custom

## 1. Créer une VM

🌞 **Créez une VM**
```bash
az vm create --name VM1 --resource-group Efrei --image Ubuntu2204 --custom-data /home/pastoufler/cours/cloud/cloud-init.txt

{
  "fqdns": "",
  "id": "/subscriptions/9ac17899-3bab-40ad-8c50-41e1ea589515/resourceGroups/Efrei/providers/Microsoft.Compute/virtualMachines/VM1",
  "location": "northeurope",
  "macAddress": "00-0D-3A-66-0A-CE",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.4",
  "publicIpAddress": "4.210.66.235",
  "resourceGroup": "Efrei",
  "zones": ""
}
```

## 2. Configurer la VM

➜ **Connectez-vous en SSH à la VM** pour effectuer les configurations qui vont suivre

🌞 **Installer docker à la machine**

```bash
pastoufle@VM1:~$ sudo docker run hello-world 
Hello from Docker!


sudo systemctl enable docker
```

🌞 **Améliorer la configuration du serveur SSH**

```bash
Protocol 2
PermitRootLogin no
X11Forwarding no
PrintLastLog yes
PermitEmptyPasswords no
LoginGraceTime 30
StrictModes yes
AllowUsers pastoufle
MaxAuthTries 6
SyslogFacility AUTH
LogLevel INFO
```

## 3. Créer une image custom

🌞 **Pour ça, suivez les commandes `az` suivantes :**

➜ **l'ID de l'image**
```bash
/subscriptions/9ac17899-3bab-40ad-8c50-41e1ea589515/resourceGroups/Efrei/providers/Microsoft.Compute/galleries/efreigallery/images/efreidef/versions/1.0.0
```


## 3. Créer une nouvelle VM
# II. Tester : spécialiser les VMs au lancement
🌞 **Sur votre PC, écrivez un fichier `cloud-init.txt`**

```yml
#cloud-config
users:
  - name: pastoufle
    primary_group: pastoufle
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$abpoPsy31MgieVIA$CLXKiAMF5mP7ckPMzMvtpSWRwObb6LaqUb1mL9xQsxU.cYCDUV0XNS2ghmBUnm4aPybRyFu6i1A14ktzB6F6V/
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDbZCbvFRU/HB0+q87Cq0RlRuK04YhohJu2u1tV8FmWUe2ZOUBSZnK2EjlqA10896W+kwAq+sYYi40c8KSTsE6rphuYTcEyarjgoGxrb2+WEEeTOSMStK/VnisHlIZaZctGgqChfNfEgX/Cre>

runcmd:
  - |
    # Lance le conteneur Nginx
    docker run -d -p 80:80 nginx

```

🌞 **Avec une comande `az`, créez une nouvelle machine**

```
az vm create --resource-group Efrei --name VM3 --image "/subscriptions/9ac17899-3bab-40ad-8c50-41e1ea589515/resourceGroups/Efrei/providers/Microsoft.Compute//galleries/efreigallery/images/efreidef/versions/1.0.0" --security-type TrustedLaunch --custom-data /home/pastoufler/cours/cloud/cloud-init.tx
```

j'ai bien ouvetrt le port 80 de ma machine azure

```bash
curl 4.210.115.57

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
