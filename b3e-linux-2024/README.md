# B3 Linux

Sur ce dépôt git vous trouverez tous les cours/TP sur le cours de Linux, année 2024-2025.

# [TP](./tp/README.md)

- [**TP1** : Advanced basics](./tp/1/README.md)

# Mémos

- [Mémo réseau Rocky Linux](./cours/memo/rocky.md)

![Power](./img/power.jpeg)