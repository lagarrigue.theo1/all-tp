# Part III : Storage is still disks in 2025

> Titre de la partie en référence au fait que le cloud est partout mais bon, c'est toujours des disques durs derrière ça n'a pas bougé. Un skill donc toujours primordial.

Dans cette partie, on joue avec le stockage de la machine. Au menu : mumuse avec des disques et des partitions, formatage, scénario de remplissage de partition.

## Index

- [Part III : Storage is still disks in 2025](#part-iii--storage-is-still-disks-in-2025)
  - [Index](#index)
  - [1. LVM](#1-lvm)
  - [2. HELP my partition is full](#2-help-my-partition-is-full)
  - [3. Prepare another partition](#3-prepare-another-partition)

![Partitions](./img/partition.png)

## 1. LVM

*LVM* (pour *Logical Volume Manager*) est l'outil de référence aujourd'hui sous Linux pour créer et gérer les partitions des disques.

> Il a beaucoup beaucoup trop de features de fou, il se contente pas de couper des disques !

🌞 **Afficher l'état actuel de LVM**

- afficher la liste des *PV* (*Volume Volumes*)
  - ce sont les disque durs et partitions physiques que LVM gère
- afficher la liste des *VG* (*Volume Groups*)
  - on regroupe les *PV* en des groupes appelés *VG*
- afficher la liste des *LV* (*Logical Volumes*)
  - les *VG* sont découpés en *LV*
  - un *LV* est une partition utilisable
  
```bash
[user@base ~]$  sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl_vbox
  PV Size               <29.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              7423
  Free PE               0
  Allocated PE          7423
  PV UUID               hTCQQm-fq20-yAcf-dGLq-ZqvD-wQZ3-JYAeKR
```

```bash
[user@base ~]$  sudo vgdisplay
  --- Volume group ---
  VG Name               rl_vbox
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  5
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                4
  Open LV               4
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <29.00 GiB
  PE Size               4.00 MiB
  Total PE              7423
  Alloc PE / Size       7423 / <29.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               5fpX9H-kD0U-kNFM-kTT5-DdDm-8r34-zqxpOR
```

```bash
[user@base ~]$  sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/rl_vbox/var
  LV Name                var
  VG Name                rl_vbox
  LV UUID                FVJTLd-04Sy-Bgcf-cdbr-cwkM-DQkS-Fwnau5
  LV Write Access        read/write
  LV Creation host, time vbox, 2025-02-17 12:31:14 +0100
  LV Status              available
  # open                 1
  LV Size                5.00 GiB
  Current LE             1280
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
   
  --- Logical volume ---
  LV Path                /dev/rl_vbox/swap
  LV Name                swap
  VG Name                rl_vbox
  LV UUID                M96Goe-TBSw-aSdS-kiJB-V3uE-a0vm-tZn2xf
  LV Write Access        read/write
  LV Creation host, time vbox, 2025-02-17 12:31:14 +0100
  LV Status              available
  # open                 2
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1
   
  --- Logical volume ---
  LV Path                /dev/rl_vbox/root
  LV Name                root
  VG Name                rl_vbox
  LV UUID                GQft1x-RBds-WgHU-Ns4r-QVZN-KWmX-dt8JL2
  LV Write Access        read/write
  LV Creation host, time vbox, 2025-02-17 12:31:14 +0100
  LV Status              available
  # open                 1
  LV Size                <13.00 GiB
  Current LE             3327
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/rl_vbox/home
  LV Name                home
  VG Name                rl_vbox
  LV UUID                OEod6J-PDV9-BsYS-2NB5-xqEJ-CHrw-fnnZej
  LV Write Access        read/write
  LV Creation host, time vbox, 2025-02-17 12:31:14 +0100
  LV Status              available
  # open                 1
  LV Size                10.00 GiB
  Current LE             2560
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:3
```

🌞 **Déterminer le type de système de fichiers**

- de la partition montée sur `/`
- de la partition montée sur `/home`
- **attention** : 
  - j'attends une commande qui détecte le type de système de fichiers sur une partition donnée : `<COMMANDE> /dev/chemin/partition`
  - je ne VEUX PAS une commande qui affiche les partitions actuellement utilisées où on voit le système de fichiers utilisé (pas de `mount` par exemple)

```bash
[user@base ~]$ lsblk -f /dev/sda
NAME             FSTYPE      FSVER    LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
sda                                                                                               
├─sda1           xfs                        d27c31ad-5028-470e-bc72-c198457b5824    645.6M    33% /boot
└─sda2           LVM2_member LVM2 001       hTCQQm-fq20-yAcf-dGLq-ZqvD-wQZ3-JYAeKR                
  ├─rl_vbox-root ext4        1.0            2627e8b0-2d2f-440f-8a1c-d87d471c84c2     10.8G    10% /
  ├─rl_vbox-swap swap        1              6ba76768-2837-475b-99e4-81794bbe977d                  [SWAP]
  ├─rl_vbox-var  xfs                        c2746e08-eb31-44a1-8cbc-c1ff1f6e3a87      4.7G     4% /var
  └─rl_vbox-home xfs                        5e917a4d-94f7-43d3-9be9-906fb4de2c27      9.8G     1% /home
```

## 2. HELP my partition is full
🌞 **Remplissez votre partition `/home`**

- on va simuler avec un truc bourrin :

```
dd if=/dev/zero of=/home/<TON_USER>/bigfile bs=4M count=2500
```

> 2500x4M ça fait 20G. Ca fait trop.

🌞 **Constater que la partition est pleine**

- avec un `df -h`
  
```bash
[user@base ~]$ dd if=/dev/zero of=/home/user/bigfile bs=4M count=2500
2500+0 records in
2500+0 records out
10485760000 bytes (10 GB, 9.8 GiB) copied, 4.72028 s, 2.2 GB/s
[user@base ~]$ df -h
Filesystem                Size  Used Avail Use% Mounted on
devtmpfs                  4.0M     0  4.0M   0% /dev
tmpfs                     888M     0  888M   0% /dev/shm
tmpfs                     355M  5.1M  350M   2% /run
/dev/mapper/rl_vbox-root   13G  1.3G   11G  11% /
/dev/mapper/rl_vbox-var   5.0G  210M  4.8G   5% /var
/dev/mapper/rl_vbox-home   10G  9.9G   73M 100% /home
/dev/sda1                 960M  315M  646M  33% /boot
tmpfs                     178M     0  178M   0% /run/user/1000
```

🌞 **Agrandir la partition**

- avec des commandes LVM il faut agrandir le logical volume
- ensuite il faudra indiquer au système de fichier ext4 que la partition a été agrandie
- prouvez avec un `df -h` que vous avez récupéré de l'espace en plus

🌞 **Remplissez votre partition `/home`**

- on va simuler encore avec un truc bourrin :

```
dd if=/dev/zero of=/home/<TON_USER>/bigfile bs=4M count=2500
```

> 2500x4M ça fait toujours 20G. Et ça fait toujours trop.

➜ **Eteignez la VM et ajoutez lui un disque de 40G**

🌞 **Utiliser ce nouveau disque pour étendre la partition `/home` de 20G**

- dans l'ordre il faut :
- indiquer à LVM qu'il y a un nouveau PV dispo
- ajouter ce nouveau PV au VG existant
- étendre le LV existant pour récupérer le nouvel espace dispo au sein du VG
- indiquer au système de fichier ext4 que la partition a été agrandie
- prouvez avec un `df -h` que vous avez récupéré de l'espace en plus

```bash
Filesystem                Size  Used Avail Use% Mounted on
devtmpfs                  4.0M     0  4.0M   0% /dev
tmpfs                     888M     0  888M   0% /dev/shm
tmpfs                     355M  5.1M  350M   2% /run
/dev/mapper/rl_vbox-root   13G  1.3G   11G  11% /
/dev/sda1                 960M  315M  646M  33% /boot
/dev/mapper/rl_vbox-var   5.0G  210M  4.8G   5% /var
/dev/mapper/rl_vbox-home   30G   11G   20G  34% /home
tmpfs                     178M     0  178M   0% /run/user/1000
```

## 3. Prepare another partition

Pour la suite du TP, on va préparer une dernière partition. Il devrait vous rester 20G de libre avec le disque de 40 que vous venez d'ajouter.

**Cette partition contiendra des fichiers HTML pour des sites web (fictifs).**

🌞 **Créez une nouvelle partition**

- le LV doit s'appeler `web`
- elle doit faire 20G et être formatée en ext4
- il faut la monter sur `/var/www`

```bash
sudo lvcreate -L 19.9G rl_vbox -n web
sudo mkfs -t ext4 /dev/rl_vbox/web
sudo mkdir /var/www
sudo mount /dev/rl_vbox/web /var/www
```

🌞 **Proposez au moins une option de montage**

- au moment où on monte la partition (avec fstab ou la commande `mount`), on peut choisir des options de montage
- proposez au moins une option de montage qui augmente le niveau de sécurité lors de l'utilisation de la partition
- je rappelle que la partition ne contiendra que des fichiers HTML

```bash
sudo nano /etc/fstab

/dev/mapper/rl_vbox-web /var/www  ext4  defaults,noexec 0 0

sudo umount /etc/fstab
sudo mount -av /var/www
mount
df -h
```
