# TP1 : Advanced basics

Dans ce TP, on refait un tour de plein de fondamentaux d'administration d'un système Linux, et on en profite pour aborder des détails à droite à gauche.

Si vous êtes (ou devenez) à l'aise avec les objectifs de ce TP, vous êtes familiers avec l'administration Linux au sens large.

On garde toujours l'aspect sécurité dans un coin de la tête.

## Sommaire

- [TP1 : Advanced basics](#tp1--advanced-basics)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [Suite du TP](#suite-du-tp)

## 0. Prérequis

➜ **Un `iso` de Rocky Linux**

- le TP commence par une installation de l'OS de façon maîtrisée

➜ **Administration des VMs en SSH**

- dès que possible vous n'utilisez QUE des connexions SSH
- je veux pas voir l'interface console sauf quand strictement nécessaire

➜ **Cartes réseau des VMs**

- vous gérez votre bail, mais votre VM doit :
  - avoir un accès internet
  - avoir accès à un réseau local dans lequel y'a votre PC aussi (pour `ping`, `ssh`, etc.)
- sur VBox je recommande :
  - une carte réseau NAT pour l'accès internet
  - une deuxième carte réseau host-only pour le réseau local

## Suite du TP

➜ [**Part I** : Rocky install](./part1.md)

➜ [**Part II** : Networking](./part2.md)

➜ [**Part III** : Storage is still disks in 2025](./part3.md)

➜ [**Part IV** : User management](./part4.md)

➜ [**Part V** : OpenSSH Server](./part5.md)

➜ [**Part VI** : Webserver and Reverse proxy](./part6.md)
