# Part I : Rocky install

Dans cette partie, vous allez simplement dérouler l'installation de Rocky, en suivant les instructions que je vous donne pour la conf.

Une fois l'installation terminée, vous pourrez éteindre la VM, et elle vous servira de base pour tous nos TPs : dès qu'un TP nécessite une VM, vous pourrez cloner celle-ci.

## Index

- [Part I : Rocky install](#part-i--rocky-install)
  - [Index](#index)
  - [1. Install instructions](#1-install-instructions)
  - [2. Proofs](#2-proofs)

## 1. Install instructions

➜ **Créer la VM dans votre hyperviseur avec :**

- un disque de 20G
- une carte réseau permettant un accès internet
- 1024M ou 2048M de RAM car l'installation est graphique (on peut tranquillement passer sur 512M de RAM après)
```bash
lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL
cat /etc/sudoers | grep wheel
groups $USER
echo $LANG
systemctl status firewalld
```
![Install Linux](./img/install.png)

➜ **L'OS doit être en anglais**

- allez j'ai même plus besoin d'argumenter ça, si ?
- n'oubliez pas de configurer une disposition clavier azerty (fin en fonction de votre clavier quoi)

➜ **Le fuseau horaire configuré doit être cohérent**

- nous c'est Europe/Paris

➜ **Une carte réseau permettant un accès internet doit être allumée**

- la carte NAT de VBox ça fait le taff

➜ **Utilisateur `root`**

- vous définissez un password que vous oubliez pas svp

➜ **Utilisateur administratif**

- vous vous créez un utilisateur, avec un mot de passe
- y'a une case à cocher pour "faire de cet utilisateur un administrateur", cochez-la

➜ **Partitionnement**

- vous configurez le schéma de partitionnement suivant :


| Point de montage | Taille       | FS    |
| ---------------- | ------------ | ----- |
| /                | 10G          | ext4  |
| /home            | 5G           | ext4  |
| /var             | 5G           | ext4  |
| swap             | 1G           | swap  |
| espace libre     | ce qui reste | aucun |

➜ **Lancez l'install !**

➜ **Une fois l'installation terminée**

- connectez-vous à la VM
- et effectuez les commandes suivantes :

```bash
# désactivation temporaire de SELinux
sudo setenforce 0

# désactivation définitive de SELinux
sudo sed -i 's/enforcing/permissive/' /etc/selinux/config

# mise à jour du système si besoin
sudo dnf update -y

# installation de paquets qu'on sera ptet amenés à use
sudo dnf install -y traceroute vim bind-utils tcpdump nano nc epel-release
```

➜ **Une fois que c'est fait, éteignez la VM et vous la rallumez jamais**

- vous cloner cette VM pour la suite du TP
- vous la clonerez dès que nécessaire si on a besoin de VM

## 2. Proofs

Pour répondre à chaque soleil, une seule ligne de commande suffit. Abusez des syntaxes `cat TRUC | grep TRUC` par exemple pour ne montrer que ce qui est demandé.

➜ **Cloner donc la VM qu'on vient d'installer, créez la machine `node1.tp1.b3`**

- et tu te connectes direct en SSH avec ton utilisateur (avec `root` c'est désactivé par défaut sous Rocky)
- sans SSH tu pourras pas faire de copier/coller, ~~ni pour copier/coller bêtement ce que te dit chatGPT, ni~~ pour écrire mon compte-rendu
- s'il faut tu configures une IP vitefé, j'donne [les instructions pour le faire à la partie 2](./part2.md) si tu as besoin de le faire now

> Appelez-moi si vous galérez à vous co en SSH, ~~bande de noobs.~~, il **faut** que ça fonctionne, c'est pas une option. Et il **faut** que tu comprennes que c'est pas une option et que c'est important, que c'est la base. Et il **faut** que tu maîtrises ça, je te le ré-expliquerai autant de fois que nécessaire.

🌞 **Prouvez que le schéma de partitionnement a bien été appliqué**

- genre les partitions qu'on a défini à l'install
- une commande qui affiche toutes les partitions en cours d'utilisation
- ainsi que l'espace disponible sur chacune des partitions

```bash
[user@node1 ~]$ sudo fdisk -l
[sudo] password for user: 
Disk /dev/sda: 20 GiB, 21474836480 bytes, 41943040 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x8af678eb

Device     Boot    Start      End  Sectors Size Id Type
/dev/sda1           2048 10487807 10485760   5G 83 Linux
/dev/sda2       10487808 18876415  8388608   4G 83 Linux
/dev/sda3       18876416 20973567  2097152   1G 82 Linux swap / Solaris
/dev/sda4       20973568 41943039 20969472  10G  f W95 Ext'd (LBA)
/dev/sda5  *    20975616 41943039 20967424  10G 83 Linux
```

➜ Par défaut, sous Rocky Linux :

- il existe un groupe appelé `wheel` déjà créé à l'installation
- le groupe `wheel` est déjà dans la conf `sudo` pour autoriser ses membres à utiliser les droits de `root` avec la commande `sudo`

🌞 **Mettre en évidence la ligne de configuration `sudo` qui concerne le groupe `wheel`**

- avec un `cat TRUC | grep TRUC` je veux voir que la bonne ligne
  ```bash
  [user@node1 ~]$ sudo cat /etc/sudoers | grep wheel
  ## Allows people in group wheel to run all commands
  %wheel	ALL=(ALL)	ALL
  # %wheel	ALL=(ALL)	NOPASSWD: ALL
  ```

🌞 **Prouvez que votre utilisateur est bien dans le groupe `wheel`**

```bash
[user@node1 ~]$ id user
uid=1000(user) gid=1000(user) groups=1000(user),10(wheel)
```

🌞 **Prouvez que la langue configurée pour l'OS est bien l'anglais**

- je veux une ligne de commande qui affiche la langue actuelle de l'OS
- que vos messages d'erreur soient en anglais ça me suffit pas ;D
  
```bash
[user@node1 ~]$ sudo locale
LANG=en_US.UTF-8
```

🌞 **Prouvez que le firewall est déjà actif**

- le service de firewalling s'appelle `firewalld` sous Rocky (on le manipule avec la commande `firewall-cmd`)
- 
```bash
[user@node1 ~]$ sudo systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; preset: enabled)
     Active: active (running) since Mon 2025-02-17 11:32:13 CET; 9min ago
       Docs: man:firewalld(1)
   Main PID: 594 (firewalld)
      Tasks: 2 (limit: 11113)
     Memory: 43.6M
        CPU: 621ms
     CGroup: /system.slice/firewalld.service
             └─594 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid

Feb 17 11:32:13 node1.tp1.b3 systemd[1]: Starting firewalld - dynamic firewall daemon...
Feb 17 11:32:13 node1.tp1.b3 systemd[1]: Started firewalld - dynamic firewall daemon.
```

```bash
[user@node1 ~]$ sudo firewall-cmd
sudo firewall-cmd
State: running

No options specified
usage: 'firewall-cmd --help' for usage information or see firewall-cmd(1) man page
```