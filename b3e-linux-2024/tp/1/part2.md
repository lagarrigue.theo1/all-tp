# Part II : Networking

Le réseau c'est la porte d'entrée pour toutes les autres machines. C'est le seul moyen d'être attaqué à distance.

Maîtriser au mieux le réseau d'une machine est donc primordial pour prétendre en renforcer la sécurité.

## Index

- [Part II : Networking](#part-ii--networking)
  - [Index](#index)
  - [1. Basic networking conf](#1-basic-networking-conf)
    - [A. Static IP](#a-static-ip)
    - [B. Hostname](#b-hostname)
  - [2. Listening ports](#2-listening-ports)
  - [3. Firewalling](#3-firewalling)

## 1. Basic networking conf

### A. Static IP

🌞 **Attribuer l'adresse IP `10.1.1.11/24`** à la VM

- ça veut dire que votre PC a pour adresse IP `10.1.1.X/24` (il est dans le même réseau)
- je vous file les instructions pour la définition de l'IP dans la VM, avec Rocky Linux on peut faire comme ça pour la définition d'une IP statique :

```bash
# on commence par repérer le nom de la carte réseau à configurer
$ ip a # on suppose dans la suite qu'on veut configurer enp0s8

# on se déplace dans le dossier de conf réseau
$ cd /etc/sysconfig/network-scripts

# création d'un fichier qui porte dans son nom le nom de l'interface réseau
$ sudo nano ifcfg-enp0s8

# le contenu du fichier est le suivant :
$ sudo cat ifcfg-enp0s8
DEVICE=enp0s8 # le nom de la carte
NAME=lan      # un nom arbitraire pas trop chiant à taper

BOOTPROTO=static # static ou dhcp
ONBOOT=yes       # la carte s'allume automatiquement au démarrage

IPADDR=10.1.1.11
NETMASK=255.255.255.0

# on indique à NetworkManager, le programme qui gère le réseau, qu'on a modifié la conf
$ sudo nmcli con reload

# on allume la carte réseau et applique la nouvelle conf
$ sudo nmcli con up lan # on réutilise le nom qu'on a mis dans le fichier
```

```bash
[user@node1 network-scripts]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7b:d4:26 brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fd00::a00:27ff:fe7b:d426/64 scope global dynamic mngtmpaddr 
       valid_lft 86399sec preferred_lft 14399sec
    inet6 fe80::a00:27ff:fe7b:d426/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:59:c5:42 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.107/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 559sec preferred_lft 559sec
    inet6 fe80::c69f:dce0:b0e1:b37b/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```

### B. Hostname

🌞 **Attribuer le nom `node1.tp1.b3` à la VM**

- ça se fait avec une commande `hostnamectl` en 2025 svp

```bash
sudo hostnamectl set-hostname node1.tp1.b3
```

## 2. Listening ports

🌞 **Déterminer la liste des programmes qui écoutent sur un port TCP**

```
ssh
```

🌞 **Déterminer la liste des programmes qui écoutent sur un port UDP**

```
NA
```

## 3. Firewalling

![fw](./img/fw.png)

➜ **Vous pouvez afficher l'état actuel de `firewalld`, le firewall de Rocky Linux, avec :**


🌞 **Pour chacun des ports précédemment repérés...**

- montrez qu'il existe une règle firewall qui autorise le trafic entrant sur ce port
- ou pas ?
```bash
[user@node1 network-scripts]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

> **Attention !** Le firewall de Rocky Linux, `firewalld`, a deux concepts pour ouvrir un port TCP/UDP. Soit on ouvre... un port avec `--add-port` et on le voit apparaître devant `ports:`. Soit on ouvre un "service" avec `--add-service` et on le voit apparaître devant `services:`. Chaque "service" est donc un port ouvert (et à fermer potentiellement à la question suivante ;) ).

🌞 **Fermez tous les ports inutilement ouverts dans le firewall**

- principe du moindre privilège encore et encore !
- pas besoin qu'un port soit ouvert si aucun service n'écoute dessus
  
  ```bash
  [user@node1 network-scripts]$ sudo firewall-cmd --remove-service dhcpv6-client
  success
  [user@node1 network-scripts]$ sudo firewall-cmd --list-all
  public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
  ```

🌞 **Pour toutes les applications qui sont en écoute sur TOUTES les adresses IP**

- dans Linux, ce sont les applications qui écoutent sur la pseudo-adresse IP `0.0.0.0` : ça signifie que toutes les adresses IP de la machine sont concernées
- modifier la configuration de l'application pour n'écouter que une seule IP : celle qui est nécessaire