# Part VI : Webserver and Reverse proxy

**Le serveur Web, un classique de l'administration Linux**.

Ici on va ajouter NGINX comme reverse proxy devant notre application Web. Il faudra un deuxième VM pour cette section :

- la première VM utilitsée jusqu'ici portera la serveur web
- la deuxième VM agira comme reverse proxy

![mostly](./img/mostly.jpg)

## Index

- [Part VI : Webserver and Reverse proxy](#part-vi--webserver-and-reverse-proxy)
  - [Index](#index)
  - [1. Webserver](#1-webserver)
  - [2. Reverse proxy](#2-reverse-proxy)
  - [3. HTTPS](#3-https)
  - [4. Bonus : dear TLS](#4-bonus--dear-tls)

## 1. Webserver

> Toute cette section est à réaiser sur une seule des deux VMs

🌞 **Installer NGINX**

- NGINX peut agir comme serveur web
- installer NGINX, puis démarrez-le
- pensez à ouvrir le bon port dans le firewall

🌞 **Prouver que le serveur Web est fonctionnel**

- faites un `curl` depuis votre PC
- vous devriez avoir la page d'accueil

🌞 **Créer un nouveau site web**

- créez un nouveau dossier dans `/var/www/` et appelez le `super_site`
- créez un simple fichier `index.html` à l'intérieur avec le contenu de votre choix
- il doit respecter le principe du moindre privilège :
  - appartenir à un user spécifique
  - appartenir à un groupe spécifique
  - des permissions les plus restrictives possibles
- ajustez la configuration de NGINX pour qu'il serve ce nouveau site web
- supprimez la configuration qui concerne le site par défaut de NGINX

🌞 **Prouvez que le nouveau site web peut être visité**

- toujours avec un `curl` depuis votre machine

## 2. Reverse proxy

> Pour cette partie, passez sur une deuxième VM fraîchement clonée. Vous pouvez dérouler votre script développé à la partie précédente.

🌞 **Installer NGINX**

- NGINX peut agir comme reverse proxy
- installer NGINX, puis démarrez-le
- pensez à ouvrir le bon port dans le firewall

🌞 **Proposer une configuration minimale**

- NGINX doit agir comme reverse proxy pour accéder au site web de la première VM
- 

## 3. HTTPS

![tls](./img/tls.jpg)

Enfin, on propose **du chiffrement au client.**

On reste sur la machine reverse proxy ici.

🌞 **Générer une clé et un certificat auto-signé**

- stockez-les à un endroit standard
- dans Rocky Linux, il existe un dossier standard pour les clés, et un pour les certificats

🌞 **Ajuster la configuration du reverse proxy**

- il doit utiliser votre clé et votre certificat
- écoute sur le port 443
- propose une connexion avec TLS d'activé

## 4. Bonus : dear TLS

⭐ **Bonus : faites en sorte d'avoir une connexion vérifiée sur le client**

- on parle d'avoir un cadenas vert si on visite en navigateur
- vous pouvez utiliser la méthode de votre choix
- je vous recommande de créer une CA

⭐ **Bonus : harden**

- proposer de la configuration additionnelle pour renforcer la sécurité de la connexion à votre serveur Web
- ça peut toucher à TLS ou autre chose

⭐ **Bonus : mTLS**

- TLS ça peut aussi mTLS pour mutual TLS
- une extension du protocole qui permet au client de s'authentifier auprès du serveur (et pas seulement l'inverse)
- extrêmement fort pour limiter l'accès à un site web donné
- mettre en palce du mTLS sur le reverse proxy : le client doit posséder un certificat pour accéder à la page
