# Part V : OpenSSH Server

**Le serveur OpenSSH est strictement nécessaire à l'administration, et occupe aussi une place cruciale dans le niveau de sécurité d'une machine.**

En effet, on parle d'un programme qui tourne en `root` (obligé...), qui écoute sur un port réseau (il est donc attaquable, c'est une porte potentiellement ouverte), et qui en plus, bah sert à prendre le contrôle d'une machine à distance.

Besoin d'un dessin pour expliquer à quel point c'est sensible ?

Néanmoins nécessaire partout.

## Index

- [Part V : OpenSSH Server](#part-v--openssh-server)
  - [Index](#index)
  - [1. Basics](#1-basics)
  - [2. Authentication modes](#2-authentication-modes)
    - [A. Key-based authentication](#a-key-based-authentication)
  - [3. Bonus : Cert-based authentication](#3-bonus--cert-based-authentication)
  - [4. Further hardening](#4-further-hardening)
  - [5. fail2ban](#5-fail2ban)
  - [6. Automatisation](#6-automatisation)

## 1. Basics

🌞 **Afficher l'identifiant du processus serveur OpenSSH en cours d'exécution**

- listez tous les programmes en cours d'exécution (avec une commande `ps`)
- mettez en évidence uniquement la ligne qui concerne le serveur SSH (y'en a qu'une)

> On peut aussi obtenir l'info avec un `systemctl status` bien senti ;D

```bash
[user@node1 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Tue 2025-02-18 15:15:53 CET; 1h 14min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 802 (sshd)
      Tasks: 1 (limit: 11091)
     Memory: 6.3M
        CPU: 105ms
     CGroup: /system.slice/sshd.service
             └─802 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Feb 18 15:15:53 node1.tp1.b3 systemd[1]: Starting OpenSSH server daemon...
```

🌞 **Changer le port d'écoute du serveur OpenSSH**

- prouvez que votre changement a pris effet
- prouvez que vous pouvez toujours vous connecter à la machine en SSH, sur ce nouveau port
- expliquez pourquoi on considère parfois utile de changer le port d'écoute par défaut du serveur SSH

```bash
[user@node1 ~]$ sudo nano /etc/ssh/sshd_config
on met le port 2222

[user@node1 ~]$ sudo systemctl restart sshd

[user@node1 ~]$ systemctl status sshd
Feb 18 17:08:13 node1.tp1.b3 sshd[809]: Server listening on 0.0.0.0 port 2222.

[user@node1 ~]$ sudo firewall-cmd --permanent --zone=public --add-port=2222/tcp

pastoufle@H4CK3R:~$ ssh user@192.168.56.109 -p 2222
Last login: Tue Feb 18 17:06:02 2025

Il y a des bot qui scan touts les serveurs et qui regarde si il y a un server openssh qui tourne
```

## 2. Authentication modes

### A. Key-based authentication

> Un classique ! Vous **devez** être à l'aise avec ça. Jamais trop tard pour s'y mettre.

🌞 **Configurer une authentification par clé**

- vous devez pouvoir vous connecter sur votre utilisateur
- sans saisir de password
- en utilisant une paire de clés

```bash
ssh-copy-id user@192.168.56.109

pastoufle@H4CK3R:~$ ssh user@192.168.56.109 -p 2222
Last login: Tue Feb 18 17:08:20 2025 from 192.168.56.1
[user@node1 ~]$ 
```

🌞 **Désactiver la connexion par password**

```bash
PasswordAuthentication no
PubkeyAuthentication yes

pastoufle@H4CK3R:~$ ssh meow@192.168.56.109 -p 2222
meow@192.168.56.109: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
```

🌞 **Désactiver la connexion en tant que `root`**

```bash
PermitRootLogin no
```

![ssh as root](./img/ssh_as_root.png)

## 3. Bonus : Cert-based authentication

> Moins classique, mais supporté depuis très longtemps par OpenSSH, et très fort en terme de sécurité !

⭐ **BONUS** : **Configurer une authentification par certificat**

- j'ai dit par certificat, pas par simple clé
- pareil, faites-le avec votre utilisateur pour les tests

> L'authentification par certificat est toujours plus forte que l'authentification par simple clé : les deux parties (typiquement, le client et le serveur) doivent prouver l'identité à l'autre. De plus, le certificat ne peut pas être falsifié, du moins si on utilise une autorité de certification digne de confiance. L'idée du certificat : on va signer la clé du client avec la clé d'une autorité de certification. Ainsi, la clé n'est plus falsifiable, l'autorité de certification peut attester que c'est la bonne clé pour le bon client.

## 4. Further hardening

🌞 **Proposer au moins 5 configurations supplémentaires qui permettent de renforcer la sécurité du serveur OpenSSH**

> Je vous recommande fooooortement de vous inspirer de ressources d'Internet pour ça. Regardez par exemple le guide de l'ANSSI à ce sujet (obsolète, mais la plupart des principes sont toujours valides), ou encore le guide CIS sur le sujet, ou l'excellent guide Mozilla sur le sujet, . Il existe d'autres ressources de confiance, à votre meilleur moteur de recherches !

## 5. fail2ban

> Un outil extrêmement récurrent dans le monde Linux : un premier rempart contre les attaques de bruteforce.

🌞 **Installer fail2ban sur la machine**

🌞 **Configurer fail2ban**

- en cas de multiples tentatives de connexion échouées sur le serveur SSH, l'utilisateur sera banni
- précisément : après 7 tentatives de connexion échouées en moins de 5 minutes
- c'est l'adresse IP de la personne qui fait des connexions échouées de façon répétée qui est blacklistée

🌞 **Prouvez que fail2ban est effectif**

- faites-vous ban
- montrez l'état de la jail fail2ban pour voir quelles IP sont ban
- levez le ban avec une commande adaptée

## 6. Automatisation

Dernière section : un peu de dév en bash pour automatiser toute la configuration que vous venez de faire.

L'idée est simple : écrire un script shell qui applique la configuration de cette Partie V (openSSH et fail2ban) sur une machine Rocky Linux fraîchement installée.

🌞 **Ecrire le script `harden.sh`**

- il doit vérifier que le serveur openSSH est démarré 
- il doit vérifier que le port d'écoute de openSSH n'est pas 22
- il doit effectuer les configurations openSSH relatives à la sécurité
  - je fais référence aux points 2. et 3.
- il doit vérifier que fail2ban est installé et démarré
- il doit vérifier que fail2ban surveille bien les logs de openSSH
 