# Part IV : User management

**Hum, cette partie est censée être envoyée vite fait bien fait ! Prouvez-le moi :D**

Gestion d'utilisateurs, de mot de passe, et de `sudo` ! Puis dans un deuxième temps, on continue sur la gestion de permissions.

## Index

- [Part IV : User management](#part-iv--user-management)
  - [Index](#index)
  - [1. Users](#1-users)
    - [A. Master what already exists](#a-master-what-already-exists)
    - [B. User creation and configuration](#b-user-creation-and-configuration)
    - [C. Hackers gonna hack](#c-hackers-gonna-hack)
  - [2. Files and permissions](#2-files-and-permissions)
    - [A. Listing POSIX permissions](#a-listing-posix-permissions)
    - [B. Protect a file using permissions](#b-protect-a-file-using-permissions)
    - [C. Extended attributes](#c-extended-attributes)

## 1. Users

### A. Master what already exists

🌞 **Déterminer l'existant :**

- lister tous les utilisateurs créés sur la machine
- lister tous les groupes d'utilisateur
- déterminer la liste des groupes dans lesquels se trouvent votre utilisateur

```bash
[user@node1 ~]$ cat /etc/passwd

[user@node1 ~]$ groups
user wheel

[user@node1 ~]$ id
uid=1000(user) gid=1000(user) groups=1000(user),10(wheel) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

🌞 **Lister tous les processus qui sont actuellement en cours d'exécution, lancés par `root`**

```bash
[user@node1 ~]$ ps -u root
    PID TTY          TIME CMD
      1 ?        00:00:01 systemd
      2 ?        00:00:00 kthreadd
      3 ?        00:00:00 pool_workqueue_
      4 ?        00:00:00 kworker/R-rcu_g
      5 ?        00:00:00 kworker/R-rcu_p
      .......
```

🌞 **Lister tous les processus qui sont actuellement en cours d'exécution, lancés par votre utilisateur**

```bash
[user@node1 ~]$ ps -u user
    PID TTY          TIME CMD
   1302 ?        00:00:00 systemd
   1304 ?        00:00:00 (sd-pam)
   1311 tty1     00:00:00 bash
   1344 ?        00:00:00 sshd
   1345 pts/0    00:00:00 bash
   1884 pts/0    00:00:00 ps
```

🌞 **Déterminer le hash du mot de passe de `root`**

```bash
[user@node1 ~]$ sudo cat /etc/shadow | grep 'root'
root:$6$1j7NHf.OCULnEFar$57tHPoIb/AUGfYNT/dP61g5.7bZOwMn3M8yPtgb0dXU85i.n/qmVgNNym/6qHfQAtSUVMhZLxOGP5fML04GV2.::0:99999:7:::

$57tHPoIb/AUGfYNT/dP61g5.7bZOwMn3M8yPtgb0dXU85i.n/qmVgNNym/6qHfQAtSUVMhZLxOGP5fML04GV2.
```

🌞 **Déterminer le hash du mot de passe de votre utilisateur**

```bash
[user@node1 ~]$ sudo cat /etc/shadow | grep 'user'
user:$6$oI7eYB8/WqT67EZZ$Zwm07aQoSnrWeJ1NmcY1uYMFUomabo.FgRkilCov5c6beXgmBuIVIlNC3YvmPEo5Tc7lRnt4c1mgPMxYY78bx/::0:99999:7:::

$Zwm07aQoSnrWeJ1NmcY1uYMFUomabo.FgRkilCov5c6beXgmBuIVIlNC3YvmPEo5Tc7lRnt4c1mgPMxYY78bx/
```

🌞 **Déterminer la fonction de hachage qui a été utilisée**

```bash
$6 SHA-512
```

🌞 **Déterminer, pour l'utilisateur `root`** :

- son shell par défaut
- le chemin vers son répertoire personnel

```bash
[user@node1 ~]$ sudo cat /etc/passwd | grep "root"
root:x:0:0:root:/root:/bin/bash

shell default : /bin/bash
repertoir personel : /root
```

🌞 **Déterminer, pour votre utilisateur** :

- son shell par défaut
- le chemin vers son répertoire personnel

```bash
[user@node1 ~]$ sudo cat /etc/passwd | grep "user"
user:x:1000:1000:user:/home/user:/bin/bash

shell default : /bin/bash
repertoir personel : /home/user
```

🌞 **Afficher la ligne de configuration du fichier `sudoers` qui permet à votre utilisateur d'utiliser `sudo`**

```bash
[user@node1 ~]$ sudo cat /etc/sudoers | grep "wheel"
## Allows people in group wheel to run all commands
%wheel	ALL=(ALL)	ALL
# %wheel	ALL=(ALL)	NOPASSWD: ALL
```

![sudo](./img/sudo.png)

### B. User creation and configuration

🌞 **Créer un utilisateur :**

- doit s'appeler `meow`
- ne doit appartenir QUE à un groupe nommé `admins`
- ne doit pas avoir de répertoire personnel utilisable
- ne doit pas avoir un shell utilisable

> Il s'agit donc ici d'un utilisateur avec lequel on pourra pas se connecter à la machine (ni en console, ni en SSH).

```bash
sudo useradd -M -N -g admins -s /usr/sbin/nologin meow
```

🌞 **Configuration `sudoers`**

- ajouter une configuration `sudoers` pour que l'utilisateur `meow` puisse exécuter seulement et uniquement les commandes `ls`, `cat`, `less` et `more` en tant que votre utilisateur
- ajouter une configuration `sudoers` pour que les membres du groupe `admins` puisse exécuter seulement et uniquement la commande `apt` en tant que `root`
- ajouter une configuration `sudoers` pour que votre utilisateur puisse exécuter n'importe quel commande en tant `root`, sans avoir besoin de saisir un mot de passe
- prouvez que ces 3 configurations ont pris effet (vous devez vous authentifier avec le bon utilisateur, et faire une commande `sudo` qui doit fonctioner correctement)

> Pour chaque point précédent, c'est une seule ligne de configuration à ajouter dans le fichier `sudoers` de la machine.

```bash

meow ALL=(user) /bin/ls, /bin/cat, /bin/less, /bin/more

%admins ALL=(ALL) usr/bin/apt

user ALL=(ALL) NOPASSWD:ALL

sudo su -s /bin/bash - meow
```

### C. Hackers gonna hack

🌞 **Déjà une configuration faible ?**

- l'utilisateur `meow` est en réalité complètement `root` sur la machine hein là. Prouvez-le.
- proposez une configuration similaire, sans présenter cette faiblesse de configuration
  - vous pouvez ajouter de la configuration
  - ou supprimer de la configuration
  - du moment qu'on garde des fonctionnalités à peu près équivalentes !
  - 
```bash
[meow@node1 user]$ sudo -u user less /etc/profile
sh-5.1$ whoami
user
sh-5.1$ sudo su - root
Last login: Mon Feb 17 18:51:38 CET 2025 on pts/0
[root@node1 ~]# whoami
root
```

```bash
ne pas mettre NOPASSWD pour l'utilisateur user
```

## 2. Files and permissions

**Dans un OS, en particulier Linux, on dit souvent que "tout est fichier".**

En effet, que ce soit les programmes (que ce soit `ls`, ou Firefox, ou Steam, ou le kernel), les fichiers personnels, les fichiers de configuration, et bien d'autres, **l'ensemble des composants d'un OS, et tout ce qu'on peut y ajouter se résume à un gros tas de fichiers.**

Gérer correctement les permissions des fichiers est une étape essentielle dans le renforcement d'une machine.

**C'est la première barrière de sécurité, (beaucoup) trop souvent négligée, alors qu'elle est extrêmement efficace et robuste.**

### A. Listing POSIX permissions

🌞 **Déterminer les permissions des fichiers/dossiers...**

- le fichier qui contient la liste des utilisateurs
  
  ```bash
  [user@node1 ~]$ ls -la /etc/passwd
  -rw-r--r--. 1 root root 1015 Feb 17 18:24 /etc/passwd
  ```

- le fichier qui contient la liste des hashes des mots de passe des utilisateurs

```bash
  [user@node1 ~]$ ls -la /etc/shadow
  ----------. 1 root root 873 Feb 17 20:41 /etc/shadow
```

- le fichier de configuration du serveur OpenSSH
- 
```bash
  [user@node1 ~]$ ls -la /etc/ssh/sshd_config
  -rw-------. 1 root root 3667 Nov  5 04:37 /etc/ssh/sshd_config
```

- le répertoire personnel de l'utilisateur `root`
```bash
  dr-xr-x---.   3 root root  4096 Feb 17 14:32 root
```
- le répertoire personnel de votre utilisateur
```bash
  drwx------.  3 user user  126 Feb 17 17:37 user
```
- le programme `ls`
```bash
 -rwxr-xr-x.  1 root root  140952 Nov  6 17:29  ls
```
- le programme `systemctl`
```bash
-rwxr-xr-x.  1 root root  305744 Nov 16 02:22  systemctl
```

> POSIX c'est le nom d'un standard qui regroupe plein de concepts avec lesquels vous êtes finalement déjà familiers. Les permissions rwx qu'on retrouve sous les OS Linux (et MacOS, et BSD, et d'autres) font partie de ce standard et sont donc appelées "permissions POSIX".

![Windows POSIX](./img/posix_compliant.png)

### B. Protect a file using permissions

🌞 **Restreindre l'accès à un fichier personnel**

- créer un fichier nommé `dont_readme.txt` (avec le contenu de votre choix)
- il doit se trouver dans un dossier lisible et écrivable par tout le monde
- faites en sorte que seul votre utilisateur (pas votre groupe) puisse lire ou modifier ce fichier
- personne ne doit pouvoir l'exécuter
- prouvez que :
  - votre utilisateur peut le lire
  - votre utilisateur peut le modifier
  - l'utilisateur `meow` ne peut pas y toucher
  - l'utilisateur `root` peut quand même y toucher

> C'est l'un des "superpouvoirs" de `root` : contourner les permissions POSIX (les permissions `rwx`). On verra bien assez tôt que `root` n'a pas de "superpouvoirs" mais que ces contournements sont liés à une mécanique qu'on appelle les *capabilites*. C'est pour plus tard ! :)

```bash
[user@node1 tmp]$ mv /var
[user@node1 tmp]$ touch dont_readme.txt
[user@node1 tmp]$ chmod 600 dont_readme.txt

[meow@node1 tmp]$ cat dont_readme.txt
sszad

[meow@node1 tmp]$ cat dont_readme.txt
cat: dont_readme.txt: Permission denied

[root@node1 tmp]# cat dont_readme.txt 
sszad

```

### C. Extended attributes

🌞 **Lister tous les programmes qui ont le bit SUID activé**

```bash
[user@node1 ~]$ find / -perm -4000 -type f 2>/dev/null
/usr/bin/su
/usr/bin/passwd
/usr/bin/gpasswd
/usr/bin/crontab
/usr/bin/mount
/usr/bin/newgrp
/usr/bin/chage
/usr/bin/sudo
/usr/bin/umount
/usr/sbin/grub2-set-bootflag
/usr/sbin/pam_timestamp_check
/usr/sbin/unix_chkpwd
 ```

🌞 **Rendre le fichier `dont_readme.txt` immuable**

- ça se fait avec les attributs étendus
- "immuable" ça veut dire qu'il ne peut plus être modifié DU TOUT : il est donc en read-only
- prouvez que le fichier ne peut plus être modifié par **personne**

```bash
sudo chattr +i dont_readme.txt

[root@node1 tmp]# echo "kk" >> dont_readme.txt 
bash: dont_readme.txt: Permission denied
```
