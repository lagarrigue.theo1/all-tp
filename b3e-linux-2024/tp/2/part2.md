
# Part II : Observe

**Il est possible d'observer en temps réel ce que fait un programme. On dit qu'on peut *tracer* un programme.**

Plusieurs techniques pour faire ça, suivant ce qu'on veut voir ; dans ce TP on va se concentrer sur les *syscalls*.

L'outil le plus élémentaire à connaître est `strace`. Il s'utilise en terminal et affiche tous les *syscalls*  que réalisent un processus.

On va aussi utiliser `sysdig` plus moderne et plus puissant.

## Sommaire

- [Part II : Observe](#part-ii--observe)
  - [Sommaire](#sommaire)
  - [1. strace](#1-strace)
  - [2. sysdig](#2-sysdig)
    - [A. Intro](#a-intro)
    - [B. Use it](#b-use-it)
  - [3. Bonus : Stratoshark](#3-bonus--stratoshark)

## 1. strace

Si on veut tracer un processus avec `strace`, c'est comme ça :

```bash
# pour tracer l'exécution d'un echo par exemple
$ strace echo yo
```

🌞 **Utiliser `strace` pour tracer l'exécution de la commande `ls`**

- faites `ls` sur un dossier qui contient des trucs
- mettez en évidence le *syscall* pour écrire dans le terminal le résultat du `ls`

```bash
write(1, "'Freeze Ra\303\253l.mp3'\n", 19'Freeze Raël.mp3') = 19
```

🌞 **Utiliser `strace` pour tracer l'exécution de la commande `cat`**

- faites `cat` sur un fichier qui contient des trucs
- mettez en évidence le *syscall* qui demande l'ouverture du fichier en lecture
- mettez en évidence le *syscall* qui écrit le contenu du fichier dans le terminal

```bash
[user@node1 ~]$ strace cat aa

openat(AT_FDCWD, "aa", O_RDONLY)        = 3


write(1, "zefze\303\251\n", 8zefzeé)

```

🌞 **Utiliser `strace` pour tracer l'exécution de `curl example.org`**

- vous devez utiliser une option de `strace`
- elle affiche juste un tableau qui liste tous les *syscalls*  appelés par la commande tracée, et combien de fois ils ont été appelé


```bash
% time     seconds  usecs/call     calls    errors syscall
------ ----------- ----------- --------- --------- ----------------
 32.64    0.000779           5       141           mmap
 14.20    0.000339          48         7           write
 11.31    0.000270          22        12           poll
  9.51    0.000227           3        60        14 openat
  6.28    0.000150           3        41           rt_sigaction
  5.66    0.000135           2        54           close
  4.19    0.000100           2        35           mprotect
  4.02    0.000096           2        36           read
  3.90    0.000093           2        46           fstat
  1.68    0.000040          40         1           sendto
  1.30    0.000031          31         1           recvfrom
  0.92    0.000022          22         1         1 connect
  0.67    0.000016           8         2           socket
  0.42    0.000010          10         1           getpeername
  0.42    0.000010          10         1           getsockopt
  0.34    0.000008           8         1           getsockname
  0.34    0.000008           4         2           socketpair
  0.34    0.000008           0        21           futex
  0.34    0.000008           8         1           clone3
  0.25    0.000006           2         3           rt_sigprocmask
  0.25    0.000006           3         2           getdents64
  0.21    0.000005           2         2           newfstatat
  0.17    0.000004           1         4           setsockopt
  0.17    0.000004           0         6           fcntl
  0.13    0.000003           1         2           ioctl
  0.08    0.000002           0         4           brk
  0.08    0.000002           2         1           sysinfo
  0.08    0.000002           2         1           rseq
  0.04    0.000001           0         2         1 arch_prctl
  0.04    0.000001           1         1           set_tid_address
  0.04    0.000001           1         1           set_robust_list
  0.00    0.000000           0         1           munmap
  0.00    0.000000           0         4           pread64
  0.00    0.000000           0         2         1 access
  0.00    0.000000           0         1           pipe
  0.00    0.000000           0         1           execve
  0.00    0.000000           0         2           statfs
  0.00    0.000000           0         1           prlimit64
  0.00    0.000000           0         1           getrandom
------ ----------- ----------- --------- --------- ----------------
100.00    0.002387           4       506        17 total
```

## 2. sysdig

### A. Intro

`sysdig` est un outil qui permet de faire pleiiin de trucs, et notamment tracer les *syscalls*  que le kernel reçoit.

Si on le lance sans préciser, il affichera TOUS les *syscalls*  que reçoit votre kernel.

On peut ajouter des filtres, pour ne voir que les *syscalls*  qui nous intéressent.

Par exemple :

```bash
# si on veut tracer les *syscalls*  effectués par le programme echo
sysdig proc.name=echo
```

> Il existe des tonnes et des tonnes de champs utilisables pour les filtres, on peut consulter la liste avec `sysdig -l`.

Ensuite on le laisse tourner, et si un *syscall* est appelé et que ça matche notre filtre, il s'affichera !

Pour installer sysdig, utilisez les commandes suivantes (instructions pour Rocky Linux 9) :

```bash
# mettons complètement à jour l'OS d'abord si nécessaire
sudo dnf update -y 

# installer sysdig et ses dépendances
sudo dnf install -y epel-release
sudo dnf install -y dkms gcc kernel-devel make perl kernel-headers

# redémarrer pour charger la nouvelle version du kernel si besoin (c'est automatique, juste lance un reboot)
sudo reboot

curl -SLO https://github.com/draios/sysdig/releases/download/0.39.0/sysdig-0.39.0-x86_64.rpm
sudo rpm -ivh sysdig-0.39.0-x86_64.rpm
```

### B. Use it

🌞 **Utiliser `sysdig` pour tracer les *syscalls*  effectués par `ls`**

- faites `ls` sur un dossier qui contient des trucs (pas un dossier vide)
- mettez en évidence le *syscall* pour écrire dans le terminal le résultat du `ls`

> Vous pouvez isoler à la main les lignes intéressantes : copier/coller de la commande, et des seule(s) ligne(s) que je vous demande de repérer.

```bash
sudo sysdig proc.name=ls

1411 15:07:19.663736279 1 ls (3587.3587) < write res=79 data= aa  .[0m.[01;36m'Freeze Ra..l.mp3'.[0m   .[01;31msysdig-0.39.0-x86_64.rpm.[0m.
```

🌞 **Utiliser `sysdig` pour tracer les *syscalls*  effectués par `cat`**

- faites `cat` sur un fichier qui contient des trucs
- mettez en évidence le *syscall* qui demande l'ouverture du fichier en lecture
- mettez en évidence le *syscall* qui écrit le contenu du fichier dans le terminal

```bash
sudo sysdig proc.name=cat

1171 15:14:14.844540662 1 cat (3642) < write res=8 data=zefze...
3536 15:12:57.955931033 1 cat (3636) > read fd=3(<f>/lib64/libc.so.6) size=832

```


🌞 **Utiliser `sysdig` pour tracer les *syscalls*  effectués par votre utilisateur**

- ça va bourriner sec, vu que vous êtes connectés en SSH étou
- juste pour vous éduquer un peu + à ce que fait le kernel à chaque seconde qui passe
- donner la commande pour ça, pas besoin de me mettre le résultat :d

```bash
[user@node1 ~]$ sudo sysdig user.name=user
```

![Too much](./img/doge-strace.jpg)

🌞 **Livrez le fichier `curl.scap` dans le dépôt git de rendu**

- `sysdig` permet d'enregistrer ce qu'il capture dans un fichier pour analyse ultérieure
- l'extension c'est `.scap` par convention
- **capturez les *syscalls*  effectués par un `curl example.org`**

> `sysdig` est un outil moderne qui sert de base à toute la suite d'outils de la boîte du même nom. On pense par exemple à Falco qui permet de tracer, monitorer, lever des alertes sur des *syscalls* , au sein d'un cluster Kubernetes.
>

```bash
voir dans le repo
```

## 3. Bonus : Stratoshark

Un tout nouveau tool bien stylé : [Stratoshark](https://wiki.wireshark.org/Stratoshark). L'interface de Wireshark (et ses fonctionnalités de fou) mais pour visualiser des captures de *syscalls*  (et autres).

Vous prenez pas trop la tête avec ça, mais si vous voulez vous amuser avec une interface stylée, il est là !

Vous pouvez exporter une capture `sysdig` avec `sysdig -w meo.scap proc.name=echo` par exemple, et la lire dans Stratoshark. 
