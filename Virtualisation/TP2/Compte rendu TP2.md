# TP2 : Routage, DHCP et DNS
 Sommaire

- [TP2 : Routage, DHCP et DNS](#tp2--routage-dhcp-et-dns)
- [0. Setup](#0-setup)
- [I. Routage](#i-routage)
- [II. Serveur DHCP](#ii-serveur-dhcp)
- [III. ARP](#iii-arp)
  - [1. Les tables ARP](#1-les-tables-arp)
  - [2. ARP poisoning](#2-arp-poisoning)

# 0. Setup

Pour chaque VM que vous créerez tout au long du TP :

- [x] hostname node1
- [x] hostname routeur
- [x] connexion ssh

# I. Routage

**Topo 1**

➜ **Tableau d'adressage**

ip a (machine node):
```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e5:fa:59 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee5:fa59/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:84:cd:44 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.101/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe84:cd44/64 scope link
       valid_lft forever preferred_lft forever
```

ip a (machine routeur):
```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f4:40:3b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef4:403b/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0c:93:15 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.102/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe0c:9315/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c9:e1:36 brd ff:ff:ff:ff:ff:ff
    inet 192.168.190.129/24 brd 192.168.190.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1490sec preferred_lft 1490sec
    inet6 fe80::8da1:d020:16cf:5d80/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

☀️ **Configuration de `router.tp2.efrei`**

```bash
[pastoufle@localhost ~]$ ping google.fr
PING google.fr (142.250.75.227) 56(84) bytes of data.
64 bytes from par10s41-in-f3.1e100.net (142.250.75.227): icmp_seq=1 ttl=128 time=32.4 ms
64 bytes from par10s41-in-f3.1e100.net (142.250.75.227): icmp_seq=2 ttl=128 time=33.9 ms
^C
--- google.fr ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 32.353/33.147/33.942/0.794 ms
[pastoufle@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f4:40:3b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef4:403b/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0c:93:15 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.102/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe0c:9315/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c9:e1:36 brd ff:ff:ff:ff:ff:ff
    inet 192.168.190.129/24 brd 192.168.190.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1737sec preferred_lft 1737sec
    inet6 fe80::a00:27ff:fec9:e136/64 scope link
       valid_lft forever preferred_lft forever
```

☀️ **Configuration de `node1.tp2.efrei`**

- configurer de façon statique son IP
  - voir l'IP demandée dans le tableau d'adressage juste au dessus
- prouvez avec une commande `ping` que `node1.tp2.efrei` peut joindre `router.tp2.efrei`

```bash
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=2.19 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=2.87 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1005ms
rtt min/avg/max/mdev = 2.187/2.527/2.868/0.340 ms
```

- ajoutez une route par défaut qui passe par `router.tp2.efrei`
```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```
- prouvez que vous avez un accès internet depuis `node1.tp2.efrei` désormais, avec une commande `ping`
```bash
PING google.fr (142.250.75.227) 56(84) bytes of data.
64 bytes from par10s41-in-f3.1e100.net (142.250.75.227): icmp_seq=1 ttl=127 time=32.4 ms
64 bytes from par10s41-in-f3.1e100.net (142.250.75.227): icmp_seq=2 ttl=127 time=33.2 ms

--- google.fr ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 32.423/32.788/33.153/0.365 ms
```
- utilisez une commande `traceroute` pour prouver que vos paquets passent bien par `router.tp2.efrei` avant de sortir vers internet

```bash
[root@node ~] traceroute google.fr
traceroute to google.fr (142.250.75.227), 30 hops max, 60 byte packets
 1  _gateway (10.2.1.254)  2.011 ms  1.818 ms  1.719 ms
 2  192.168.190.2 (192.168.190.2)  2.407 ms  2.326 ms  2.552 ms
```

# II. Serveur DHCP

**__Topo 2__**

➜ **Tableau d'adressage**

☀️ **Test du DHCP** sur `node1.tp2.efrei`

➜ ip a
```bash
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:84:cd:44 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.101/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe84:cd44/64 scope link
       valid_lft forever preferred_lft forever
```

🌟 **BONUS**

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
DNS1=1.1.1.1
```
# III. ARP

## 1. Les tables ARP

```bash
[root@routeur ~]# ip neigh show
10.2.1.100 dev enp0s3 lladdr 08:00:27:e5:fa:59 STALE
192.168.190.254 dev enp0s9 lladdr 00:50:56:f5:d1:e3 STALE
192.168.190.2 dev enp0s9 lladdr 00:50:56:e7:5b:51 STALE
10.2.1.253 dev enp0s3 lladdr 08:00:27:be:c7:63 STALE
192.168.213.1 dev enp0s8 lladdr 0a:00:27:00:00:10 DELAY
```

## 2. ARP poisoning

☀️ **Exécuter un simple ARP poisoning**
```
Changer ip mac de enp0s3: 
sudo ip neigh change 10.0.1.10 lladdr aa:bb:cc:dd:ee:ff dev enp0s3

```

![APR sniffed ?](img/arp_sniff.jpg)