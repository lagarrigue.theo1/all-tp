# TP4 : ARP Poisoning

Dernier TP un peu plus fun. Assez simple, à vous de choisir si vous voulez aller plus loin.

- [TP4 : ARP Poisoning](#tp4--arp-poisoning)
  - [I. Setup](#i-setup)
  - [II. Premiers pas sur l'attaque](#ii-premiers-pas-sur-lattaque)
  - [III. Aller plus loin](#iii-aller-plus-loin)
    - [1. Scapy](#1-scapy)
    - [2. Pousser l'attaque](#2-pousser-lattaque)

## I. Setup

Il vous faudra trois machines :

- deux victimes
- un attaquant

Je vous laisse libre des détails concernant le setup, vous pouvez :

- utiliser GNS ou juste utiliser un réseau local avec votre hyperviseur (host-only avec VirtualBox)
- mettre en place un setup plus réaliste avec une des deux victimes qui est un routeur
  - le routeur donne accès internet à la victime
  - ainsi vous vous positionnez entre la victime et le routeur
- utiliser une distribution Linux orientée sécu peut être une bonne idée (Kali, BlackArch, etc.)

Assurez-vous que tout le monde se ping avant de continuer.

## II. Premiers pas sur l'attaque

Pour les premiers pas sur l'attaque, on va utiliser juste deux outils :

- `arping` : ptit outil en ligne de commande qui permet d'envoyer des trames ARP
- Wireshark : pour visualiser la mise en place de l'attaque

Le principe de l'attaque pour rappel :

- envoyer des ARP request unsolicited en boucle à nos deux victimes
  - on indique au routeur que l'IP de la victime correspond à notre MAC (l'attaquant)
  - on indique à la victime que l'IP du routeur correpond à notre MAC (l'attaquant)
- une fois en place, on intercepte tout le trafic entre la victime et le routeur

➜ **Poisoning basique**

- **depuis l'attaquant, utiliser une commande [`arping`](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)**
  - utilisez-la pour écrire des données arbitraires dans la table ARP de la victime
  - vous pouvez vraiment forcer l'écriture de n'importe quoi, amusez vous avec la commande
  - genre la mac `aa:aa:aa:aa:aa:aa` qui correspond à l'IP `10.10.10.10.` peu importe, faites des tests
- **depuis la victime**
  - constatez les changements dans la table ARP
  - sous Linux c'est `ip neighbor show` pour voir la table ARP
  - on peut abréger cette commande en `ip n s`
- **depuis l'attaquant**
  - utilisez Wireshark pour voir les trames envoyées

![ARP Poisoning](./img/ARP_poisoning.svg)

## III. Aller plus loin

Pour aller plus loin, je ne connais pas de petit tool qui fait le café et fait tout, et c'est de toute façon plus amusant et formateur de le faire soi-même.

Le meilleur moyen reste donc de développer. Je recommande Python et la librairie Scapy.

Avec la librairie Scapy il est très facile, en une seule ligne de code, d'envoyer une trame complètement arbitraire sur le réseau.

C'est donc une libairie parfaite pour mettre en place une attaque réseau comme l'ARP poisoning.

### 1. Scapy

Le procédé que je vous recommande est le suivant :

➜ **Setup votre environnement de travail**

- [**Python**](https://www.python.org/downloads/) installé
- librairie [**Scapy**](https://scapy.readthedocs.io/en/latest/installation.html) installée (avec une commande `pip install`)
- un **IDE** (comme VSCode)

➜ **Apprendre à juste envoyer des trames basiques avec Scapy**

- des ping par exemple
  - la commande `ping` envoie des paquets de type ICMP
  - les paquets ICMP servent à plein de trucs, et il y a un identifiant pour chaque fonction, c'est un entier qui est indiqué dans le paquet ICMP
  - type `8` : *echo request* : c'est le ping
  - type `0` : *echo reply* : c'est le pong

> Les autres types ICMP servent à d'autres trucs que la commande `ping`. Voyez ICMP comme un protocole de diagnostic réseau.

- jouer avec ARP

> Gardez **Wireshark** ouvert pour tout le temps avoir un oeil sur ce qu'il se passe concrètement.

➜ Mettre en place la même attaque qu'avec `arping`

### 2. Pousser l'attaque

Pour aller plus loin sur l'attaque, comme on a discuté plus tôt en cours, le classique c'est :

- **ARP poisoning sur la victime et le routeur**
- agir soi-même comme un routeur de façon transparente, et faire suivre les trames vers les vrais destinataires (man-in-the-middle)
- **intercepter les requêtes DNS du client** qui sont en clair
- répondre de façon malicieuse
- guider la victime vers un site de phishing

Pour ce faiiire :

- il faudra encore utiliser **Scapy**
- je vous aiderai évidemment, mais commencez par Google, il y a énormément de code qui explique comment faire tout ça :)
