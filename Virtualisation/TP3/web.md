
# II. Serveur Web

- [II. Serveur Web](#ii-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Page HTML et racine web](#2-page-html-et-racine-web)
  - [3. Config de NGINX](#3-config-de-nginx)
  - [4. Firewall](#4-firewall)
  - [5. Test](#5-test)

## 1. Installation
🌞 **Installation du serveur web NGINX**

- installez le paquet `nginx`

  ```
  sudo dnf install nginx
  ```
## 2. Page HTML et racine web

🌞 **Création d'une bête page HTML**

- créez le dossier `/var/www/efrei_site_nul/`

  ```
  sudo mkdir /var/www/efrei_site_nul/
  ```

- faites le appartenir à l'utilisateur `nginx` (sinon le contenu du dossier ne sera pas accessible par le serveur Web NGINX, et il ne pourra pas servir le site !)
  ```
  sudo chmod nginx:nginx efrei_site_nul
  ```

## 3. Config de NGINX

🌞 **Création d'un fichier de configuration NGINX**

- fichier `/etc/nginx/conf.d/web.net2.tp3.conf` le contenu est :

  ```nginx
    server {
        # on indique le nom d'hôte du serveur
        server_name   web.net2.tp3;

        # on précise sur quelle IP et quel port on veut que le site soit dispo
        listen        10.3.2.101:80;

        location      / {
            # on indique l'endroit où se trouve notre racine web
            root      /var/www/efrei_site_nul;

            # et on indique le nom de la page d'accueil, pour pas que le client ait besoin de le préciser explicitement
            index index.html;
        }
    }
  ```

## 4. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

- autorisé port 80 firefall
  ```
  sudo firewall-cmd --add-port=80/tcp --permanent
  sudo firewall-cmd --reload
  ```

- lister
  ```
  [root@web /]# sudo firewall-cmd --list-all
  public (active)
    target: default
    icmp-block-inversion: no
    interfaces: enp0s3 enp0s8
    sources:
    services: cockpit dhcpv6-client ssh
    ports: 80/tcp
    protocols:
    forward: yes
    masquerade: no
    forward-ports:
    source-ports:
    icmp-blocks:
    rich rules:
  ```

## 5. Test

🌞 **Test local**

- vous pouvez visiter le site web en local, depuis la ligne de commande de la machine `web.net2.tp3`, avec la commande `curl` : par exemple `curl http://10.3.2.101`

  ```
  [root@node1 ~]# curl http://10.3.2.101
  Salut a tous
  ```


🌞 **Avec un nom ?**

- utilisez le fichier `hosts` de votre machine client pour accéder au site web en saissant `http://web.net2.tp3` (ce qu'on avait écrit dans la conf quoi !)

- on dois modifier le fichier hosts de note1.net1.tp3
  ```
  [root@node1 ~]# cat /etc/hosts
  127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
  ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
  10.3.2.101 web.net2.tp3
  ```

- test final
- ```
  [root@node1 ~]# curl http://web.net2.tp3
  Salut a tous
  ```