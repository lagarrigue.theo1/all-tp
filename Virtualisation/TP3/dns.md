# III. Serveur DNS

- [III. Serveur DNS](#iii-serveur-dns)
  - [1. Firewall](#1-firewall)
  - [4. Test](#4-test)
  - [5. DHCP my old friend](#5-dhcp-my-old-friend)

## 1. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

- ouvrire le port 53 TCP
  ```
  sudo firewall-cmd --add-port=53/udp --permanent
  sudo firewall-cmd --reload
  ```
- check
  ```
  [root@dns ~]# firewall-cmd --list-all
  public (active)
    target: default
    icmp-block-inversion: no
    interfaces: enp0s3 enp0s8
    sources:
    services: cockpit dhcp dhcpv6-client ssh
    ports: 53/udp
    protocols:
    forward: yes
    masquerade: no
    forward-ports:
    source-ports:
    icmp-blocks:
    rich rules:
  ```
## 4. Test


🌞 **Depuis l'une des machines clientes du réseau 1** (par exemple `node1.net1.tp3`)

- utiliser `dig` pour trouver à quelle IP correspond le nom `web.net2.tp3`
- On dois add DNS1 dans notre fichier /etc/sysconfig/network-scripts/ifcfg-enp0s3
  ```
  nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

  NAME=enp0s3
  DEVICE=enp0s3

  BOOTPROTO=dcp
  ONBOOT=yes

  DNS1=10.3.2.102
  ```


  ```
  [root@node1 ~]# dig web.net2.tp3

  ; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38896
  ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 1232
  ; COOKIE: 92171d00fff5c89b01000000651fcd90b8bfbcfa622525a7 (good)
  ;; QUESTION SECTION:
  ;web.net2.tp3.                  IN      A

  ;; ANSWER SECTION:
  web.net2.tp3.           86400   IN      A       10.3.2.101

  ;; Query time: 5 msec
  ;; SERVER: 10.3.2.102#53(10.3.2.102)
  ;; WHEN: Fri Oct 06 11:04:16 CEST 2023
  ;; MSG SIZE  rcvd: 85
  ```
- utiliser `curl` pour visiter le site web sur `web.net2.tp3` en utilisant son nom
  - assurez-vous de purger votre fichier `hosts` de vos éventuelles précédentes modifications

  ```
  [root@node1 ~]# curl http://web.net2.tp3
  Salut a tous
  ```

## 5. DHCP my old friend

🌞 **Editez la configuration du serveur DHCP sur `dhcp.net1.tp3`**

- l'adresse du serveur DNS qui est donnée au client doit désormais être celle de `dns.net2.tp3` (il faut bien préciser une IP, pas le nom)

- on dois eddit la conf du dhcp pour mettre notre dns par default
  ```
  # DHCP Server Configuration file.
  #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
  #   see dhcpd.conf(5) man page
  #
  # create new
  # specify domain name
  option domain-name     "srv.world";
  # specify DNS server's hostname or IP address
  option domain-name-servers     dlp.srv.world;
  # default lease time
  default-lease-time 600;
  # max lease time
  max-lease-time 7200;
  # this DHCP server to be declared valid
  authoritative;
  # specify network address and subnetmask
  subnet 10.3.1.0 netmask 255.255.255.0 {
      # specify the range of lease IP address
      range dynamic-bootp 10.3.1.50 10.3.1.99;
      # specify broadcast address
      option broadcast-address 10.3.1.255;
      # specify gateway
      option routers 10.3.1.254;
      # specify dns
      option domain-name-servers 10.3.2.102;
  }
  ```

- prouvez que ça fonctionne avec un `dig` depuis un client qui a fraîchement récupéré une IP

  ```
  [root@node1 ~]# dig web.net2.tp3

  ; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43452
  ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 1232
  ; COOKIE: 0a51b783de5f7e8801000000651fcf299831b2263030f478 (good)
  ;; QUESTION SECTION:
  ;web.net2.tp3.                  IN      A

  ;; ANSWER SECTION:
  web.net2.tp3.           86400   IN      A       10.3.2.101

  ;; Query time: 7 msec
  ;; SERVER: 10.3.2.102#53(10.3.2.102)
  ;; WHEN: Fri Oct 06 11:11:05 CEST 2023
  ;; MSG SIZE  rcvd: 85
  ```
