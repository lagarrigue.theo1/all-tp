# I. DHCP

🌞 **Preuve !**

- effectuer une demande d'adresse IP depuis `node1.net1.tp3`
- montrez l'IP obtenue
```
[root@node1 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d6:4c:61 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.51/24 brd 10.3.1.255 scope global dynamic enp0s3
       valid_lft 658sec preferred_lft 658sec
    inet6 fe80::54f2:b038:4b93:f3d3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a8:f9:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.101/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea8:f901/64 scope link
       valid_lft forever preferred_lft forever
[root@node1 ~]# sudo dhclient -v
dhclient(1339) is already running - exiting.

This version of ISC DHCP is based on the release available
on ftp.isc.org. Features have been added and other changes
have been made to the base software release in order to make
it work better with this distribution.

Please report issues with this software via:
https://bugs.rockylinux.org/

exiting.
[root@node1 ~]# sudo dhclient -r -v
Killed old client process
Internet Systems Consortium DHCP Client 4.4.2b1
Copyright 2004-2019 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/enp0s8/08:00:27:a8:f9:01
Sending on   LPF/enp0s8/08:00:27:a8:f9:01
Listening on LPF/enp0s3/08:00:27:d6:4c:61
Sending on   LPF/enp0s3/08:00:27:d6:4c:61
Sending on   Socket/fallback
DHCPRELEASE of 10.3.1.51 on enp0s3 to 10.3.1.253 port 67 (xid=0x2aac3070)
[root@node1 ~]# sudo dhclient -v
Internet Systems Consortium DHCP Client 4.4.2b1
Copyright 2004-2019 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/enp0s8/08:00:27:a8:f9:01
Sending on   LPF/enp0s8/08:00:27:a8:f9:01
Listening on LPF/enp0s3/08:00:27:d6:4c:61
Sending on   LPF/enp0s3/08:00:27:d6:4c:61
Sending on   Socket/fallback
DHCPDISCOVER on enp0s8 to 255.255.255.255 port 67 interval 4 (xid=0x3f100806)
DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 5 (xid=0x4d936074)
DHCPOFFER of 10.3.1.51 from 10.3.1.253
DHCPREQUEST for 10.3.1.51 on enp0s3 to 255.255.255.255 port 67 (xid=0x4d936074)
DHCPACK of 10.3.1.51 from 10.3.1.253 (xid=0x4d936074)
bound to 10.3.1.51 -- renewal in 255 seconds.
```
- montrez que votre table de routage a été mise à jour
```
[root@node1 ~]# ip route show
default via 10.3.1.254 dev enp0s3
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.51
192.168.213.0/24 dev enp0s8 proto kernel scope link src 192.168.213.101 metric 101
```
- montrez l'adresse du serveur DNS que vous utilisez

```
[root@node1 ~]# dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8571
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             281     IN      A       142.250.179.78

;; Query time: 26 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:04:00 CEST 2023
;; MSG SIZE  rcvd: 55
```
- prouvez que vous avez un accès internet normal avec un `ping`
```
[root@node1 ~]# ping google.fr
PING google.fr (142.250.178.131) 56(84) bytes of data.
64 bytes from par21s22-in-f3.1e100.net (142.250.178.131): icmp_seq=1 ttl=127 time=21.9 ms
64 bytes from par21s22-in-f3.1e100.net (142.250.178.131): icmp_seq=2 ttl=127 time=25.3 ms
^C
--- google.fr ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 21.933/23.614/25.296/1.681 ms
[root@node1 ~]#
```